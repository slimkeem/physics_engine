#ifndef SCENES_OBJECTEXAMPLES_PSUBOBJECT_EXAMPLES_H
#define SCENES_OBJECTEXAMPLES_PSUBOBJECT_EXAMPLES_H


// gmlib2
#include <gmlib2/qt/scenegraph.h>
#include <gmlib2/qt/exampleobjects/parametric/parametricobjects.h>
#include <parametric/psubspaceobject.h>

namespace examples
{

  // Declare contained examples
  void initPSubCurveOnCircleExample(gmlib2::qt::Scenegraph* scenegraph);
  void initPSubCurveOnTorusExample(gmlib2::qt::Scenegraph* scenegraph);
  void initPSubCurveOnPolygon4Example(gmlib2::qt::Scenegraph* scenegraph);
  void initPSubSurfaceOnTorusExample(gmlib2::qt::Scenegraph* scenegraph);
  void initPSubSurfaceOnPolygon4Example(gmlib2::qt::Scenegraph* scenegraph);
  void initPSubSpaceCurveOnCircleExample(gmlib2::qt::Scenegraph* scenegraph);
  void initPSubSpaceCurveOnTorusExample(gmlib2::qt::Scenegraph* scenegraph);
  void initPSubSpacePolygonSurfaceOnPolygon10Example(
    gmlib2::qt::Scenegraph* scenegraph);


  // "Local" detail resources
  // namespace detail {
  //   auto lambda_construct_my_very_special_thing = [](){};
  // } // namespace detail




  // Examples
  void initPSubCurveOnCircleExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    namespace gm2p   = gmlib2::parametric;
    namespace gm2qtp = gmlib2::qt::parametric;

    using Circle       = gm2qtp::PCircle;
    using SubCurveType = gm2qtp::PCurve<gm2p::PSubCurve<gm2p::PLine, Circle>>;
    using SCPoint      = SubCurveType::PSpaceObject_Type::Point_Type;
    using SCVector     = SubCurveType::PSpaceObject_Type::Vector_Type;

    // Circle
    auto* circle = new Circle{2.5, scenegraph};
    circle->translateLocal(Circle::Vector{-5.0, 0.0, 0.0});
    circle->initDefaultComponents();
    circle->defaultMesh()->setSamples(100);

    // Subcurve on circle
    auto  subcurve_p = SCPoint{M_PI_2};
    auto  subcurve_v = SCVector{M_PI};
    auto* subcurve   = new SubCurveType{circle, subcurve_p, subcurve_v};
    subcurve->setParent(circle);
    subcurve->translateLocal(SubCurveType::Vector{12.0, 0.0, 0.0});
    subcurve->initDefaultComponents();
    subcurve->defaultMesh()->setSamples(100);
  }

  void initPSubCurveOnTorusExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    namespace gm2p   = gmlib2::parametric;
    namespace gm2qtp = gmlib2::qt::parametric;

    using Torus = gm2qtp::PTorus;
    using SubCurveType
      = gm2qtp::PCurve<gm2p::PSubCurve<gm2p::PHermiteCurveP2V2, Torus>>;
    using SCPoint  = SubCurveType::PSpaceObject_Type::Point_Type;
    using SCVector = SubCurveType::PSpaceObject_Type::Vector_Type;

    // Torus
    auto* torus = new Torus(3.0, 1.0, 1.0, scenegraph);
    torus->initDefaultComponents();
    torus->defaultMesh()->setSamples({20, 20});

    // Subcurve on torus
    auto  subcurve_p0 = SCPoint{0.0, 0.0};
    auto  subcurve_p1 = SCPoint{2 * M_PI, 1.5 * M_PI};
    auto  subcurve_v0 = SCVector{20.0, 0.0};
    auto  subcurve_v1 = SCVector{2.0, 0.0};
    auto* subcurve    = new SubCurveType{torus, subcurve_p0, subcurve_p1,
                                      subcurve_v0, subcurve_v1};
    subcurve->setParent(torus);
    subcurve->initDefaultComponents();
    subcurve->defaultMesh()->setSamples(100);
  }

  void initPSubCurveOnPolygon4Example(gmlib2::qt::Scenegraph* scenegraph)
  {
    namespace gm2p   = gmlib2::parametric;
    namespace gm2qtp = gmlib2::qt::parametric;

    using Polygon4 = gm2qtp::PPolygon4;
    using SubCurveType
      = gm2qtp::PCurve<gm2p::PSubCurve<gm2p::PHermiteCurveP2V2, Polygon4>>;
    using SCPoint  = SubCurveType::PSpaceObject_Type::Point_Type;
    using SCVector = SubCurveType::PSpaceObject_Type::Vector_Type;

    // Polygon4
    auto* polygon4
      = new Polygon4(Polygon4::ControlPoints{Polygon4::Point{0.0, 0.0, 0.0},
                                             Polygon4::Point{4.0, 0.0, 0.0},
                                             Polygon4::Point{4.0, 0.0, 4.0},
                                             Polygon4::Point{0.0, 0.0, 4.0}},
                     scenegraph);
    polygon4->initDefaultComponents();
    polygon4->defaultMesh()->setSamples();


    // Subcurve on polygon4
    auto  subcurve_p0 = SCPoint{1.0, 0.0, 0.0, 0.0};
    auto  subcurve_p1 = SCPoint{0.0, 0.0, 1.0, 0.0};
    auto  subcurve_v0 = SCVector{-1.0, 0.0, 0.0, 1.0};
    auto  subcurve_v1 = SCVector{-1.0, 0.0, 0.0, 1.0};
    auto* subcurve    = new SubCurveType{polygon4, subcurve_p0, subcurve_p1,
                                      subcurve_v0, subcurve_v1};
    subcurve->setParent(polygon4);
    subcurve->initDefaultComponents();
    subcurve->defaultMesh()->setSamples(100);
  }

  void initPSubSurfaceOnTorusExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    namespace gm2p   = gmlib2::parametric;
    namespace gm2qtp = gmlib2::qt::parametric;

    using Torus = gm2qtp::PTorus;
    using SubSurfaceType
      = gm2qtp::PSurface<gm2p::PSubSurface<gm2p::PPlane, Torus>>;
    using SSPoint  = SubSurfaceType::PSpaceObject_Type::Point_Type;
    using SSVector = SubSurfaceType::PSpaceObject_Type::Vector_Type;

    // Torus
    auto* torus = new Torus(3.0, 1.0, 1.0, scenegraph);
    torus->initDefaultComponents();
    torus->defaultMesh()->setSamples({20, 20});

    // Subsurface on torus
    auto  subsurface_p = SSPoint{0.2, 0.2};
    auto  subsurface_u = SSVector{0.4, 0.0};
    auto  subsurface_v = SSVector{0.0, 0.4};
    auto* subsurface
      = new SubSurfaceType{torus, subsurface_p, subsurface_u, subsurface_v};
    subsurface->setParent(torus);
    subsurface->initDefaultComponents();
    subsurface->defaultMesh()->setSamples({20, 20});
    subsurface->translateLocal(SubSurfaceType::Vector{2.0, 0.0, 0.0});
  }

  void initPSubSurfaceOnPolygon4Example(gmlib2::qt::Scenegraph* scenegraph)
  {
    namespace gm2p   = gmlib2::parametric;
    namespace gm2qtp = gmlib2::qt::parametric;

    using Polygon4 = gm2qtp::PPolygon4;
    using SubSurfaceType
      = gm2qtp::PSurface<gm2p::PSubSurface<gm2p::PPlane, Polygon4>>;
    using SSPoint  = SubSurfaceType::PSpaceObject_Type::Point_Type;
    using SSVector = SubSurfaceType::PSpaceObject_Type::Vector_Type;

    // Polygon4
    auto* polygon4
      = new Polygon4(Polygon4::ControlPoints{Polygon4::Point{0.0, 0.0, 0.0},
                                             Polygon4::Point{4.0, 0.0, 0.0},
                                             Polygon4::Point{4.0, 0.0, 4.0},
                                             Polygon4::Point{0.0, 0.0, 4.0}},
                     scenegraph);
    polygon4->initDefaultComponents();
    polygon4->defaultMesh()->setSamples();

    // Surface on polygon4
    auto  subsurface_p = SSPoint{1.0, 0.0, 0.0, 0.0};
    auto  subsurface_u = SSVector{-1.0, 1.0, 0.0, 0.0} * 0.5;
    auto  subsurface_v = SSVector{0.25 - 1.0, 0.25, 0.25, 0.25} * 0.5;
    auto* subsurface
      = new SubSurfaceType{polygon4, subsurface_p, subsurface_u, subsurface_v};
    subsurface->setParent(polygon4);
    subsurface->initDefaultComponents();
    subsurface->defaultMesh()->setSamples({20, 20});
    subsurface->translateLocal(SubSurfaceType::Vector{0.0, 4.0, 0.0});
  }


  void initPSubSpaceCurveOnCircleExample(gmlib2::qt::Scenegraph* scenegraph)
  {

    namespace gm2p   = gmlib2::parametric;
    namespace gm2qtp = gmlib2::qt::parametric;

    using Circle       = gm2qtp::PCircle;
    using SubCurveType = gm2qtp::PCurve<gm2p::PSubCurve<gm2p::PLine, Circle>>;
    using SCPoint      = SubCurveType::PSpaceObject_Type::Point;
    using SCVector     = SubCurveType::PSpaceObject_Type::Vector;

    // Torus
    auto* circle = new Circle(3.0, scenegraph);
    circle->initDefaultComponents();
    circle->defaultMesh()->setSamples(5);
    //    circle->defaultMesh()->setEnabled(false);

    // Subcurve on torus
    auto  subcurve_p = SCPoint{M_PI};
    auto  subcurve_v = SCVector{M_PI};
    auto* subcurve   = new SubCurveType{circle, subcurve_p, subcurve_v};
    subcurve->setParent(circle);
    subcurve->initDefaultComponents();
    subcurve->defaultMesh()->setSamples(20);
    subcurve->translateLocal(SubCurveType::Vector{0.0, 4.0, 0.0});
  }

  void initPSubSpaceCurveOnTorusExample(gmlib2::qt::Scenegraph* scenegraph)
  {

    namespace gm2p   = gmlib2::parametric;
    namespace gm2qtp = gmlib2::qt::parametric;

    using Torus = gm2qtp::PTorus;
    //    using SubCurvePSpaceCurveType
    //      = gm2p::PHermiteCurveP2V2<Torus::ParametricObjectEmbedBase, 1>;
    //    using SubCurveType = gm2qtp::PCurve<
    //      gm2p::ParametricSubObject<SubCurvePSpaceCurveType, Torus>>;
    using SubCurveType = gm2qtp::PCurve<gm2p::PSubCurve<gm2p::PLine, Torus>>;
    //    using SubCurveType = gm2qtp::PCurve<
    //      gm2p::PSubCurve<gm2p::PHermiteCurveP2V2, Torus>>;
    using SCPoint  = SubCurveType::PSpaceObject_Type::Point;
    using SCVector = SubCurveType::PSpaceObject_Type::Vector;

    // Torus
    auto* torus = new Torus(3.0, 1.0, 1.0, scenegraph);
    torus->initDefaultComponents();
    torus->defaultMesh()->setSamples({20, 20});
    torus->defaultMesh()->setEnabled(false);

    // Subcurve on torus
    auto  subcurve_p = SCPoint{M_PI, M_PI};
    auto  subcurve_v = SCVector{M_PI, M_PI};
    auto* subcurve   = new SubCurveType{torus, subcurve_p, subcurve_v};
    //    auto  subcurve_p0 = SCPoint{0.0, 0.0};
    //    auto  subcurve_p1 = SCPoint{2 * M_PI, 1.5 * M_PI};
    //    auto  subcurve_v0 = SCVector{20.0, 0.0};
    //    auto  subcurve_v1 = SCVector{2.0, 0.0};
    //    auto* subcurve    = new SubCurveType{torus, subcurve_p0, subcurve_p1,
    //                                      subcurve_v0, subcurve_v1};
    subcurve->setParent(torus);
    subcurve->initDefaultComponents();
    subcurve->defaultMesh()->setSamples(100);
    subcurve->translateLocal(SubCurveType::Vector{0.0, 4.0, 0.0});
  }

  void initPSubSpaceSurfaceOnTorusExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    namespace gm2p   = gmlib2::parametric;
    namespace gm2qtp = gmlib2::qt::parametric;

    using Torus = gm2qtp::PTorus;
    using SubSurfaceType
      = gm2qtp::PSurface<gm2p::PSubSurface<gm2p::PPlane, Torus>>;
    using SSPoint  = SubSurfaceType::PSpaceObject_Type::Point_Type;
    using SSVector = SubSurfaceType::PSpaceObject_Type::Vector_Type;

    // Torus
    auto* torus = new Torus(3.0, 1.0, 1.0, scenegraph);
    torus->initDefaultComponents();
    torus->defaultMesh()->setSamples({20, 20});

    // Subsurface on torus
    auto  subsurface_p = SSPoint{0.2, 0.2};
    auto  subsurface_u = SSVector{0.4, 0.0};
    auto  subsurface_v = SSVector{0.0, 0.4};
    auto* subsurface
      = new SubSurfaceType{torus, subsurface_p, subsurface_u, subsurface_v};
    subsurface->setParent(torus);
    subsurface->initDefaultComponents();
    subsurface->defaultMesh()->setSamples({20, 20});
    subsurface->translateLocal(SubSurfaceType::Vector{2.0, 0.0, 0.0});
  }

  void initPSubSpacePolygonSurfaceOnPolygon10Example(
    gmlib2::qt::Scenegraph* scenegraph)
  {
    {
      namespace gm2p   = gmlib2::parametric;
      namespace gm2qt  = gmlib2::qt;
      namespace gm2qtp = gm2qt::parametric;

      // Type helpers
      using PPolygon10
        = gm2qtp::PPolygonSurface<gm2p::PPolygon<10, gm2qt::SceneObject>>;
      using SubPolygonSurfaceType = gm2qtp::PPolygonSurface<
        gm2p::PSubPolygonSurface<gm2p::PPolygon, 4, PPolygon10>>;
      using SPSCPs   = SubPolygonSurfaceType::PSpaceObject_Type::ControlPoints;
      using SPSPoint = SubPolygonSurfaceType::PSpaceObject_Type::Point_Type;

      // Polygon10
      auto* polygon10 = new PPolygon10(
        PPolygon10::ControlPoints{
          PPolygon10::Point{-1.5, 0.0, 0.25},    // p0
          PPolygon10::Point{-1.5, 0.0, -1.75},   // p1
          PPolygon10::Point{0, 0.0, -1.0},       // p2
          PPolygon10::Point{1.25, 0.0, -1.5},    // p3
          PPolygon10::Point{1.5, 0.0, -0.5},     // p4
          PPolygon10::Point{2.0, 0.0, 0.75},     // p5
          PPolygon10::Point{0.75, 0.0, 1.25},    // p6
          PPolygon10::Point{0, 0.0, 2.25},       // p7
          PPolygon10::Point{-1.0, 0.0, 1.5},     // p8
          PPolygon10::Point{-2.0, 0.0, 1.75}     // p9
        },
        scenegraph);
      polygon10->initDefaultComponents();
      polygon10->defaultMesh()->setSamples();

      // Surface on polygon4
      auto sps_p0 = SPSPoint{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      auto sps_p1 = SPSPoint{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      auto sps_p2 = SPSPoint{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      auto sps_p3
        = SPSPoint{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0} / 10.0;
      auto* subpolygonsurface = new SubPolygonSurfaceType{
        polygon10, SPSCPs{sps_p0, sps_p1, sps_p2, sps_p3}};
      subpolygonsurface->setParent(polygon10);
      subpolygonsurface->initDefaultComponents();
      subpolygonsurface->defaultMesh()->setSamples(20);
      subpolygonsurface->translateLocal(
        SubPolygonSurfaceType::Vector{0.0, 0.5, 0.0});
    }
  }

}   // namespace examples

#endif   // SCENES_OBJECTEXAMPLES_PSUBOBJECT_EXAMPLES_H
