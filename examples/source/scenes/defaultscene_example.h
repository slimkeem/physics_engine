#ifndef SCENES_DEFAULTSCENE_EXAMPLE_H
#define SCENES_DEFAULTSCENE_EXAMPLE_H

// gmlib2
#include <gmlib2/qt/scenegraph.h>
#include <gmlib2/qt/exampleobjects/parametric/parametricobjects.h>

namespace examples
{

  // Declare contained examples
  void initDefaultSceneExample(gmlib2::qt::Scenegraph* scenegraph);


  // Examples
  void initDefaultSceneExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    auto* t0 = new PTorus(3.0, 1.0, 1.0, scenegraph);
    t0->initDefaultComponents();
    t0->defaultMesh()->setSamples({20, 20});
    t0->translateLocal(PTorus::Vector{5.0, 5.0, 0.0});

    auto* t1 = new PSphere(4.0, scenegraph);
    t1->initDefaultComponents();
    t1->defaultMesh()->setSamples({20, 20});
    t1->translateLocal(PSphere::Vector{5.0, -5.0, 0.0});

    auto* t2 = new PPlane(PPlane::Point{-3.0, -3.0, 0.0},
                          PPlane::Vector{6.0, 0.0, 0.0},
                          PPlane::Vector{0.0, 6.0, 0.0}, t1);
    t2->initDefaultComponents();
    t2->defaultMesh()->setSamples({2, 2});
    t2->translateLocal(PPlane::Vector{-10.0, 0.0, 0.0});

    auto* t3 = new PCircle(3.0, t2);
    t3->initDefaultComponents();
    t3->defaultMesh()->setSamples(100);
    t3->translateLocal(PCircle::Vector{0.0, 10.0, 0.0});
    t3->setParent(t2);

    auto* t4 = new PLine(PLine::Point{-2.5, 0.0, 0.0},
                         PLine::Vector{5.0, 0.0, 0.0}, t2);
    t4->initDefaultComponents();
    t4->defaultMesh()->setSamples(100);
    t4->translateLocal(PLine::Vector{0.0, 10.0, 0.0});
  }

}   // namespace examples

#endif   // SCENES_DEFAULTSCENE_EXAMPLE_H
