#ifndef PCYLINDER_H
#define PCYLINDER_H

#include <gmlib2.h>
//#include "../../gmlib2/include/core/utils.h"


namespace customshapes
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 2>
  class PCylinder
    : public gmlib2::parametric::PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = gmlib2::parametric::PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor(s)
    PCylinder(Unit_Type radius = Unit_Type{1},
              Unit_Type height = Unit_Type{2})
      : m_radius{radius},
        m_height{height}{}

    template <typename... Ts>
    PCylinder(Unit_Type radius,
              Unit_Type height,
              Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_radius{radius},
        m_height{height}
    {
    }

    // Members
    Unit_Type m_radius;
    Unit_Type m_height;

    // TPSurf interface
  public:
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = gmlib2::utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };





  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PCylinder<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PBoolArray
  PCylinder<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{true, false}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PCylinder<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PSpacePoint
  PCylinder<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::startParameters()
    const
  {
    return PSpacePoint{0, 0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PCylinder<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PSpacePoint
  PCylinder<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::endParameters()
    const
  {
    return PSpacePoint{m_height, 2 * M_PI};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PCylinder<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::EvaluationResult
  PCylinder<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::evaluate(
    const PSpacePoint& par, const PSizeArray& no_der,
    const PBoolArray& /*from_left*/) const
  {
    const auto& u        = par[0];
    const auto& v        = par[1];
    const auto& no_der_u = no_der[0];
    const auto& no_der_v = no_der[1];

    EvaluationResult p(no_der_u + 1, no_der_v + 1);

    const Unit_Type r    = m_radius;
    const Unit_Type h    = m_height;
    const Unit_Type cv   = r * std::cos(v);
    const Unit_Type sv   = r * std::sin(v);
    const Unit_Type hu = h * u;

    p(0, 0) = {sv, cv, hu, 1};                                  // S
    if (no_der_u) p(1, 0) = {0, 0, 1, 0};                       // Su
    if (no_der_v) p(0, 1) = {cv, -sv, 0, 0};                    // Sv
    if (no_der_u and no_der_v) p(1, 1) = {0, 0, 0, 0};          // Suv

    return p;
  }



}   // namespace customshapes

#endif // PCYLINDER_H
