import Qt3D.Core 2.0
import Qt3D.Render 2.0

RenderSurfaceSelector {
  id: surface_selector

  property alias topLeftCamera: topLeftViewport.camera;
  property alias topRightCamera: topRightViewport.camera;
  property alias bottomLeftCamera: bottomLeftViewport.camera;
  property alias bottomRightCamera: bottomRightViewport.camera;
  property alias window: surface_selector.surface
  property alias clearColor: clear_buffers.clearColor
  property point viewportCenter: Qt.point(0.5,0.5)

  onViewportCenterChanged: {
    updateAspectRatios()
  }

  Viewport {
    id: mainViewport
    normalizedRect: Qt.rect(0, 0, 1, 1)

    ClearBuffers {
      id: clear_buffers
      buffers: ClearBuffers.ColorDepthBuffer
      clearColor: "#c0c0c0"

      NoDraw{}
    }

    ForwardRendererTechnique {
      id: topLeftViewport
      normalizedRect: Qt.rect(0, 0, viewportCenter.x, viewportCenter.y)
    }

    ForwardRendererTechnique {
      id: topRightViewport
      normalizedRect: Qt.rect(viewportCenter.x, 0, 1.0-viewportCenter.x, viewportCenter.y)
    }

    ForwardRendererTechnique {
      id: bottomLeftViewport
      normalizedRect: Qt.rect(0, viewportCenter.y, viewportCenter.x, 1.0-viewportCenter.y)
    }

    ForwardRendererTechnique {
      id: bottomRightViewport
      normalizedRect: Qt.rect(viewportCenter.x, viewportCenter.y, 1.0-viewportCenter.x, 1.0-viewportCenter.y)
    }
  }

  function updateAspectRatios() {
    var tl_ar = viewportCenter.x / viewportCenter.y
    var tr_ar = (1.0-viewportCenter.x) / viewportCenter.y
    var bl_ar = viewportCenter.x / (1.0-viewportCenter.y)
    var br_ar = (1.0-viewportCenter.x) / (1.0-viewportCenter.y)

    topLeftCamera.aspectRatio = tl_ar
    topRightCamera.aspectRatio = tr_ar
    bottomLeftCamera.aspectRatio = bl_ar
    bottomRightCamera.aspectRatio = br_ar
  }
}
