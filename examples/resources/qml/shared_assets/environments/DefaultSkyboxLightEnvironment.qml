import Qt3D.Core 2.0
import Qt3D.Render 2.9
import Qt3D.Input 2.0
import Qt3D.Extras 2.9
import QtQuick 2.0

Entity {
  id: environment

  SkyboxEntity {
      baseName: "qrc:///ext_shared_assets/qt3d/envmaps/cedar_bridge/cedar_bridge_irradiance"
      extension: ".dds"
      gammaCorrect: true
  }

  Entity {
    components: [
        EnvironmentLight {
            irradiance: TextureLoader {
                source: "qrc:///ext_shared_assets/qt3d/envmaps/cedar_bridge/cedar_bridge_irradiance.dds"

                minificationFilter: Texture.LinearMipMapLinear
                magnificationFilter: Texture.Linear
                wrapMode {
                    x: WrapMode.ClampToEdge
                    y: WrapMode.ClampToEdge
                }
                generateMipMaps: false
            }
            specular: TextureLoader {
                source: "qrc:///ext_shared_assets/qt3d/envmaps/cedar_bridge/cedar_bridge_specular.dds"

                minificationFilter: Texture.LinearMipMapLinear
                magnificationFilter: Texture.Linear
                wrapMode {
                    x: WrapMode.ClampToEdge
                    y: WrapMode.ClampToEdge
                }
                generateMipMaps: false
            }
        }
    ]
}
}
