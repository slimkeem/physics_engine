import QtQuick 2.2
import QtQuick.Controls 1.4

import com.uit.GMlib2Qt 1.0


import "windows"

import "scenarios/SingleCamera" as SingleCamera
import "scenarios/SingleCameraWithOverlay" as SingleCameraWithOverlay
import "scenarios/QuadCamera" as QuadCamera
import "scenarios/LightingDemoRoom" as LightingDemoRoom

import "scenes" as Scenes
import "scenes/ObjectExamples" as ObjectExamples


ApplicationWindow {
//EditorWindow {
  visible: true

  width: 800
  height: 600

//  SingleCamera.Scenario {
  SingleCameraWithOverlay.Scenario {
//  QuadCamera.Scenario {
//  LightingDemoRoom.Scenario{
    id: scene
    anchors.fill: parent

    Scenes.DefaultScene {}
//    ObjectExamples.PTorusObject {}
  }
}
