import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

import com.uit.GMlib2Qt 1.0

import "../shared_assets/componentdelegates"


ApplicationWindow {
  id: window
  objectName: "app window"
  visible: true

  width: 800
  height: 600

  default property alias content: scene_container_item.children
  property int right_pane_width : 50

  menuBar: MenuBar {
    Menu { title: "File"
      MenuItem { text: "&Quit"; shortcut: StandardKey.Quit; onTriggered: Qt.quit() }
    }
    Menu { title: "Pages"
      MenuItem { text: "Scene"; onTriggered: {content.state = ""} }
      MenuItem { text: "Help"; onTriggered: {content.state = "help"} }
    }
  }

  SplitView {
    objectName: "split_view"
    anchors.fill: parent
    orientation: Qt.Horizontal

    Item {
      objectName: "item"
      id: content

      Layout.fillWidth: true

      states: [
        State{ name: "help"
          PropertyChanges { target: help_page; visible: true }
          PropertyChanges { target: scene_container_item; visible: false }
        }
      ]

      Item {
        id: scene_container_item
        anchors.fill: parent
      }

      TextArea {
        objectName: "textarea"
        id: help_page
        anchors.fill: parent
        readOnly: true
        text: "Sorry, this is the best I can do ^^,"

        visible: false
      }
    }

    Item {
      id: right_pane
      width: 200
      Layout.maximumWidth: 200

      SplitView {
        anchors.fill: parent
        orientation: Qt.Vertical

        TreeView {

          height: parent.height/2
          model: scene.scenemodel

          TableViewColumn {
            title: "Name"
            role: "name"
            delegate: Text{ text: styleData.value }
          }
          TableViewColumn {
            title: "Selected"
            role: "item"
            delegate: Text{ text: styleData.value.selected?"yes":"no" }
          }

          onClicked: {
            components_view.model =
              scene.scenemodel.data(index,SceneModel.ComponentModelRole)
          }
        }
        ListView {
          id: components_view

          Layout.fillHeight: true

          delegate: ComponentDelegateLoader{}
//          delegate: ComponentDelegate{}
//          delegate: Text{text: component_type}
        }

      }
    }
  }
}
