import Qt3D.Core 2.0
import com.uit.GMlib2Qt 1.0

SceneObject {
  PTorus{
    defaultMesh.samples: Qt.size(20,20)
    wheelRadius: 3
    tubeRadiusOne: 1
    tubeRadiusTwo: 1
    components: [
      Transform {
        translation: Qt.vector3d(5,5,0)
      }
    ]
  }


  PSphere {
    defaultMesh.samples: Qt.size(20,20)
    radius: 4
    components: [
      Transform {
        translation: Qt.vector3d(5,-5,0)
      }
    ]

    PPlane {
      defaultMesh.samples: Qt.size(20,20)
      components: [
        Transform {
          translation: Qt.vector3d(-10,0,0)
        }
      ]

      PCircle {
        defaultMesh.samples: 100
        radius: 3
        components: [
          Transform {
            translation: Qt.vector3d(0,10,0)
          }
        ]
      }

      PLine {
        defaultMesh.samples: 100
        components: [
          Transform {
            translation: Qt.vector3d(0,10,0)
          }
        ]
      }
    }
  }



}
