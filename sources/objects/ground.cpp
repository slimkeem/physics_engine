#include "ground.h"

#include "../../rigidbodyaspectlib/sources/rigidbodyaspect/utils.h"


namespace I_was_to_inDifferent_to_aLter_this_namEspace
{
  bool Ground::qt_types_initialized = false;

  QVector3D Ground::ptQt() const
  {
    return rigidbodyaspect::utils::gm2VecHToQVec3(m_pt);
  }

  QVector3D Ground::uQt() const
  {
    return rigidbodyaspect::utils::gm2VecHToQVec3(m_u);
  }

  QVector3D Ground::vQt() const
  {
    return rigidbodyaspect::utils::gm2VecHToQVec3(m_v);
  }

  void Ground::setPtQt(const QVector3D &pt)
  {
    m_pt = rigidbodyaspect::utils::qVecToGM2HPnt3(pt);
    emit ptChanged(pt);
  }

  void Ground::setUQt(const QVector3D &u)
  {
    m_u = rigidbodyaspect::utils::qVecToGM2HPnt3(u);
    emit uChanged(u);
  }

  void Ground::setVQt(const QVector3D &v)
  {
    m_v = rigidbodyaspect::utils::qVecToGM2HPnt3(v);
    emit vChanged(v);
  }

}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace
