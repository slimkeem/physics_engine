#ifndef TUBE_H
#define TUBE_H

#include "../../examples/source/pcylinder.h"

//#include <gmlib2/qt/integration.h>
#include <gmlib2/qt/exampleobjects/parametric/psurface.h>

#include <QGoochMaterial>

// qt
#include <QQmlEngine>

namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

  class Tube : public gmlib2::qt::parametric::PSurface<
                 customshapes::PCylinder<gmlib2::qt::SceneObject>> {
    using Base = PSurface<customshapes::PCylinder<gmlib2::qt::SceneObject>>;
    Q_OBJECT

    Q_PROPERTY(
      gmlib2::qt::parametric::PSurfaceMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)
    Q_PROPERTY(double height MEMBER m_height NOTIFY heightChanged)
    Q_PROPERTY( Qt3DExtras::QGoochMaterial* defaultMaterial READ defaultMaterial )

    // Constructor(s)
  public:
    template <typename... Ts>
    Tube(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();

      connect(this, &Tube::radiusChanged, this->defaultMesh(),
              &gmlib2::qt::parametric::PSurfaceMesh::reSample);
      connect(this, &Tube::heightChanged, this->defaultMesh(),
              &gmlib2::qt::parametric::PSurfaceMesh::reSample);
    }

    static void registerQmlTypes(int version_major, int version_minor)
    {
      if (qt_types_initialized) return;

      constexpr auto registertype_uri = "com.uit.STE6245";
//      qDebug() << "Trying to register type <Tube>; getting id: " <<
      qmlRegisterType<Tube>(registertype_uri, version_major, version_minor,
                            "Tube");

      qt_types_initialized = true;
    }


//    void registerExampleObjectsAsQmlTypes()
//    {
//    qmlRegisterType<customshapes::PCylinder>(gmlib2::qt::init::detail::qt_registertype_uri, 1, 0,
//                                        "PCylinder");
//    }

    // Signal(s)
  signals:
    void radiusChanged();
    void heightChanged();

  private:
    static bool qt_types_initialized;
  };


}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace


#endif // TUBE_H
