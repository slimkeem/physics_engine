import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10

import com.uit.STE6245 1.0 as App
import com.uit.STE6245.RigidBody 1.0 as RB

App.Tube {
  id: tube

  property alias environment: rbc.environment

  radius: 0.5
  height: 2.0

  defaultMesh.samples: Qt.size(20,20)

  onRadiusChanged: defaultMesh.reSample()
  onHeightChanged: defaultMesh.reSample()

  RB.CylinderController{
    id: rbc

    radius: tube.radius
    height: tube.height

    onFrameComputed: tube.setFrameParentQt(dir,up,pos)
  }

  function initTranslation( vec ) {
    translateParentQt(vec)
    rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
  }

}
