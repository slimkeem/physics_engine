import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10

import com.uit.STE6245 1.0 as App
import com.uit.STE6245.RigidBody 1.0 as RB

App.Ground {
  id: roof

  property alias environment: rbc.environment

  RB.PlaneController{
    id: rbc

    p: roof.pt
    u: roof.u
    v: roof.v
  }

  function initTranslation( vec1,vec2,vec3 ) {
    setParametersQt(vec1,vec2,vec3)
    rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
  }
}
