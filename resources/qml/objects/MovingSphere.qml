import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10

import com.uit.STE6245 1.0 as App
import com.uit.STE6245.RigidBody 1.0 as RB

App.Ball {
  id: ball

  property alias environment: rbc.environment
//  property alias initialMass: rbc.initialMass
  property alias velocity: rbc.velocity

  radius: 0.5
  defaultMesh.samples: Qt.size(20,20)

  onRadiusChanged: defaultMesh.reSample()

  RB.SphereController{
    id: rbc

    radius: ball.radius
//    initialMass: 1
    velocity: Qt.vector3d(0,0,0)

    onFrameComputed: ball.setFrameParentQt(dir,up,pos)
  }

  function initTranslation( vec ) {
    translateParentQt(vec)
    rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
  }
}
