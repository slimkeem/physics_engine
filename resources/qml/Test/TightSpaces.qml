import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB
import "../objects" as Obj

SceneObject {
   RB.Environment {
      id: rba_environment
      gravity: Qt.vector3d(0,0,0)
    }

   RB.Environment {
      id: rba_environment2
      gravity: Qt.vector3d(0,10,0)
    }


   Ground {
      id: floor

      defaultMesh.samples: Qt.size(2,2)

      RB.PlaneController{
        id: floor_rbc

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.StaticObject

        p: floor.pt
        u: floor.u
        v: floor.v

      }

      Component.onCompleted: {

        // place object
        setParametersQt(Qt.vector3d(-10,0,-5),Qt.vector3d(0,0,10),Qt.vector3d(20,0,0))

        // reset plane RB-controller
        floor_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }

   Ground {
      id: r_side

      defaultMesh.samples: Qt.size(2,2)

      RB.PlaneController{
        id: r_side_rbc

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.StaticObject

        p: r_side.pt
        u: r_side.u
        v: r_side.v

      }

      Component.onCompleted: {
        // place object
        setParametersQt(Qt.vector3d(-10,0,5),Qt.vector3d(0,0,-10),Qt.vector3d(10,10,0))

        // place object
        //setParametersQt(Qt.vector3d(10,0,5),Qt.vector3d(10,10,0),Qt.vector3d(0,0,-10))

        // reset plane RB-controller
        r_side_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }



   Ball {
      id: ball1

      radius: 0.45
      defaultMesh.samples: Qt.size(20,20)

      onRadiusChanged: defaultMesh.reSample()

      RB.SphereController{
        id: ball1_rbc

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.DynamicObject

        radius: ball1.radius
//        initialMass: 1
        velocity: Qt.vector3d(-6,-6,0) //at {-3,-3,0} it just wants to slide?

        onFrameComputed: ball1.setFrameParentQt(dir,up,pos);
      }

      Component.onCompleted: {

        // initial object placement
        translateGlobalQt( Qt.vector3d(-6.3,3,0) )

        // reset sphere RB-controller
        ball1_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }
}
