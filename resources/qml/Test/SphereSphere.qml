import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB
import "../objects" as Obj

SceneObject {
   RB.Environment {
      id: rba_environment
      gravity: Qt.vector3d(0,0,0)
    }

   RB.Environment {
      id: rba_environment2
      gravity: Qt.vector3d(0,10,0)
    }

   Ground {
      id: floor

      defaultMesh.samples: Qt.size(2,2)
      defaultMaterial.diffuse: "grey"

      RB.PlaneController{
        id: floor_rbc

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.StaticObject

        p: floor.pt
        u: floor.u
        v: floor.v

      }

      Component.onCompleted: {

        // place object
        setParametersQt(Qt.vector3d(-10,-5,-5),Qt.vector3d(0,0,10),Qt.vector3d(20,0,0))

        // reset plane RB-controller
        floor_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }

   Ground {
      id: roof

      defaultMesh.samples: Qt.size(2,2)
      defaultMaterial.diffuse: "cyan"

      RB.PlaneController{
        id: roof_rbc

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.StaticObject

        p: roof.pt
        u: roof.u
        v: roof.v

      }

      Component.onCompleted: {

        // place object
        setParametersQt(Qt.vector3d(-10,5,-5),Qt.vector3d(20,0,0),Qt.vector3d(0,0,10))

        // reset plane RB-controller
        roof_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }

   Ball {
      id: ball1

      radius: 0.65
      defaultMesh.samples: Qt.size(20,20)
      defaultMaterial.diffuse: "blue"

      onRadiusChanged: defaultMesh.reSample()

      RB.SphereController{
        id: ball1_rbc

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.DynamicObject

        radius: ball1.radius
//        initialMass: 1
        velocity: Qt.vector3d(0,-30,0)

        onFrameComputed: ball1.setFrameParentQt(dir,up,pos);
      }

      Component.onCompleted: {

        // initial object placement
        translateGlobalQt( Qt.vector3d(0,1,0) )

        // reset sphere RB-controller
        ball1_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }

   Ball {
      id: ball2

      radius: 0.45
      defaultMesh.samples: Qt.size(20,20)
      defaultMaterial.diffuse: "grey"

      onRadiusChanged: defaultMesh.reSample()

      RB.SphereController{
        id: ball2_rbc

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.DynamicObject

        radius: ball2.radius
//        initialMass: 1
        velocity: Qt.vector3d(0,0,0)

        onFrameComputed: ball2.setFrameParentQt(dir,up,pos);
      }

      Component.onCompleted: {

        // initial object placement
        translateGlobalQt( Qt.vector3d(0,-1,0) )

        // reset sphere RB-controller
        ball2_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }

}
