import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB


import "../objects" as Obj

SceneObject {
  RB.Environment {
    id: env
    gravity: Qt.vector3d(0,-9.8,0)
  }

  Obj.FixedPlane {
    environment: env;
    Component.onCompleted: initTranslation
                           (Qt.vector3d(-10,-5,-5),
                            Qt.vector3d(0,0,10),
                            Qt.vector3d(20,0,0)) }

  Obj.FixedCylinder {
      environment: env;
      height: 2;
      radius: 1;
      Component.onCompleted: {
          rotateGlobalQt(1.57,Qt.vector3d(1,0,0))
          initTranslation(Qt.vector3d(0,0,0))
      }
  }

  Obj.MovingSphere {
      environment: env;
      velocity: Qt.vector3d(3,0,0);
      /*initialMass: 1;*/
      radius: 1;

      Component.onCompleted: initTranslation(Qt.vector3d(-9,-3,0)) }


}
