import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10




import "Test" as Objects

ApplicationWindow {
    id: root

    function updateGravity() {
        var g = Qt.vector3d(g_x.value,g_y.value,g_z.value)

        rba_environment.gravity = g
    }




    visible: true

    width: 800
    height: 600


    menuBar: MenuBar {
        Menu {
            title: "Scenes"
            MenuItem {
                text: "Simple"
                onTriggered: {
                    my_loader.source = "Test/SingleBallSinglePlane.qml";
                    my_loader.reload();
                }
            }
            MenuItem {
                text: "Sliding"
                onTriggered: {
                    my_loader.source = "Test/Sliding.qml";
                    my_loader.reload();
                }
            }
            MenuItem {
                text: "Tight spaces"
                onTriggered: {
                    my_loader.source = "Test/TightSpaces.qml";
                    my_loader.reload();
                }
            }
            MenuItem {
                text: "Cylinder"
                onTriggered: {
                    my_loader.source = "Test/cylinder.qml";
                    my_loader.reload();
                }
            }
            MenuItem {
                text: "Deadly mix"
                onTriggered: {
                    my_loader.source = "Test/CollisionTest.qml";
                    my_loader.reload();
                }
            }
            MenuItem {
                text: "You want balls"
                onTriggered: {
                    my_loader.source = "Test/SphereSphere.qml";
                    my_loader.reload();
                }
            }
        }
    }



    statusBar: Item {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 40

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 5
            anchors.rightMargin: 5

            Item { Layout.fillWidth: true }

            Text { text: "Toggle simulator"}
            CheckBox{ checked:  scenario.simulatorRunStatus; onCheckedChanged: scenario.simulatorRunStatus = !scenario.simulatorRunStatus}

            Item{ width: 10 }

            Button{ text:"Quick Reset"; isDefault: true; onClicked: my_loader.reload();}

        }

    }

    Scenario {

        id: scenario
        anchors.fill: parent

        EntityLoader {
            id: my_loader
            source: "Test/SingleBallSinglePlane.qml"

            function reload() {
                var reload_source = source
                source = ""
                source = reload_source
            }

        }
        //    Objects.SingleBallSinglePlane{}
        //    Objects.SphereSphere{} //simple spheresphere collision check
        //    Objects.TightSpaces{} //sphereplane innerloop and sphereplane singularities check
        //    Objects.CollisionTest{} //Deadly mix
    }
}
