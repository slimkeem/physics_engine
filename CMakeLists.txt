# Minimum CMake version required
cmake_minimum_required(VERSION 3.8)

# Global name of the project
project(STE6245_TemplateDemo VERSION 0.1 LANGUAGES CXX)

message("Blaze libraries: ${BLAZE_LIBARIES}")

#########
# Find Qt
find_package(Qt5Core 5.11 REQUIRED)
find_package(Qt5Quick 5.11 REQUIRED)
find_package(Qt53DQuick 5.11 REQUIRED)
find_package(Qt53DExtras 5.11 REQUIRED)
find_package(Qt5Gui 5.11 REQUIRED)
set(QT_TARGET_KEYWORDS
  Qt5::3DExtras
  Qt5::3DQuick
  Qt5::Quick
  Qt5::Gui
  Qt5::Core
  )

######################################
# Find GMlib2 and GMlib2 Qt extensions
find_package(gmlib2qt REQUIRED CONFIG NO_DEFAULT_PATH)


########################
# Add mechanicsaspectlib
#
# - Depends on what is set
#   prior to the
#   add_subdirectory command
include_directories(rigidbodyaspectlib/sources)



##########################
# Source files and friends
set( EX_SRCS_PREFIX  examples/source)
set( RBA_SRCS_PREFIX rigidbodyaspectlib/sources/rigidbodyaspect)
set( SRCS_PREFIX     sources)

set( HDRS
  ${EX_SRCS_PREFIX}/examples.h
  ${EX_SRCS_PREFIX}/pcylinder.h
  ${EX_SRCS_PREFIX}/scenes/examples_template.h
  ${EX_SRCS_PREFIX}/scenes/shared.h
  ${EX_SRCS_PREFIX}/scenes/defaultscene_example.h
  ${EX_SRCS_PREFIX}/scenes/objectexamples/ppoint_examples.h
  ${EX_SRCS_PREFIX}/scenes/objectexamples/phermitecurve_examples.h
  ${EX_SRCS_PREFIX}/scenes/objectexamples/pcoonspatch_examples.h
  ${EX_SRCS_PREFIX}/scenes/objectexamples/ppolygon_examples.h
  ${EX_SRCS_PREFIX}/scenes/objectexamples/psubobject_examples.h


  ${RBA_SRCS_PREFIX}/init.h
  ${RBA_SRCS_PREFIX}/constants.h
  ${RBA_SRCS_PREFIX}/types.h
  ${RBA_SRCS_PREFIX}/utils.h
  ${RBA_SRCS_PREFIX}/jobs/genericcolliderjob.h
  ${RBA_SRCS_PREFIX}/jobs/genericsimjob.h
  ${RBA_SRCS_PREFIX}/jobs/genericupdatejob.h
  ${RBA_SRCS_PREFIX}/frontend/abstractrigidbodycontroller.h
  ${RBA_SRCS_PREFIX}/frontend/planecontroller.h
  ${RBA_SRCS_PREFIX}/frontend/cylindercontroller.h
  ${RBA_SRCS_PREFIX}/frontend/spherecontroller.h
  ${RBA_SRCS_PREFIX}/frontend/simulatorsettings.h
  ${RBA_SRCS_PREFIX}/frontend/environment.h
  ${RBA_SRCS_PREFIX}/backend/rigidbodyaspect.h
  ${RBA_SRCS_PREFIX}/backend/planecontrollerbackend.h
  ${RBA_SRCS_PREFIX}/backend/cylindercontrollerbackend.h
  ${RBA_SRCS_PREFIX}/backend/spherecontrollerbackend.h
  ${RBA_SRCS_PREFIX}/backend/simulatorsettingsbackend.h
  ${RBA_SRCS_PREFIX}/backend/environmentbackend.h
  ${RBA_SRCS_PREFIX}/geometry/movingbody.h
  ${RBA_SRCS_PREFIX}/geometry/fixedbody.h
  ${RBA_SRCS_PREFIX}/geometry/rbtypes.h
  ${RBA_SRCS_PREFIX}/geometry/rigidbodycontainer.h
  ${RBA_SRCS_PREFIX}/algorithms/simulation.h
  ${RBA_SRCS_PREFIX}/algorithms/collisiondetection.h
  ${RBA_SRCS_PREFIX}/algorithms/impactresponse.h
  ${RBA_SRCS_PREFIX}/algorithms/cacheproperty.h
  ${RBA_SRCS_PREFIX}/algorithms/sortunique.h
  ${RBA_SRCS_PREFIX}/algorithms/statechange.h
  ${RBA_SRCS_PREFIX}/algorithms/collisionresolution.h
  ${RBA_SRCS_PREFIX}/algorithms/collisioninsertion.h
  ${RBA_SRCS_PREFIX}/algorithms/closestpoint.h
  ${RBA_SRCS_PREFIX}/algorithms/bookkeeping.h

  ${SRCS_PREFIX}/guiapplication.h
  ${SRCS_PREFIX}/objects/ball.h
  ${SRCS_PREFIX}/objects/tube.h
  ${SRCS_PREFIX}/objects/ground.h
  )

set( SRCS
  ${RBA_SRCS_PREFIX}/utils.cpp
  ${RBA_SRCS_PREFIX}/jobs/genericcolliderjob.cpp
  ${RBA_SRCS_PREFIX}/jobs/genericsimjob.cpp
  ${RBA_SRCS_PREFIX}/jobs/genericupdatejob.cpp
  ${RBA_SRCS_PREFIX}/frontend/abstractrigidbodycontroller.cpp
  ${RBA_SRCS_PREFIX}/frontend/planecontroller.cpp
  ${RBA_SRCS_PREFIX}/frontend/cylindercontroller.cpp
  ${RBA_SRCS_PREFIX}/frontend/spherecontroller.cpp
  ${RBA_SRCS_PREFIX}/frontend/simulatorsettings.cpp
  ${RBA_SRCS_PREFIX}/frontend/environment.cpp
  ${RBA_SRCS_PREFIX}/backend/rigidbodyaspect.cpp
  ${RBA_SRCS_PREFIX}/backend/planecontrollerbackend.cpp
  ${RBA_SRCS_PREFIX}/backend/cylindercontrollerbackend.cpp
  ${RBA_SRCS_PREFIX}/backend/spherecontrollerbackend.cpp
  ${RBA_SRCS_PREFIX}/backend/simulatorsettingsbackend.cpp
  ${RBA_SRCS_PREFIX}/backend/environmentbackend.cpp
  ${RBA_SRCS_PREFIX}/geometry/rigidbodycontainer.cpp
  ${RBA_SRCS_PREFIX}/algorithms/simulation.cpp
  ${RBA_SRCS_PREFIX}/algorithms/collisiondetection.cpp
  ${RBA_SRCS_PREFIX}/algorithms/impactresponse.cpp
  ${RBA_SRCS_PREFIX}/algorithms/cacheproperty.cpp
  ${RBA_SRCS_PREFIX}/algorithms/sortunique.cpp
  ${RBA_SRCS_PREFIX}/algorithms/statechange.cpp
  ${RBA_SRCS_PREFIX}/algorithms/collisionresolution.cpp
  ${RBA_SRCS_PREFIX}/algorithms/collisioninsertion.cpp
  ${RBA_SRCS_PREFIX}/algorithms/closestpoint.cpp  
  ${RBA_SRCS_PREFIX}/algorithms/bookkeeping.cpp

  ${SRCS_PREFIX}/main.cpp
  ${SRCS_PREFIX}/guiapplication.cpp
  ${SRCS_PREFIX}/objects/ball.cpp
  ${SRCS_PREFIX}/objects/tube.cpp
  ${SRCS_PREFIX}/objects/ground.cpp
  )

qt5_wrap_cpp( HDRS_MOC
  ${RBA_SRCS_PREFIX}/types.h
  ${RBA_SRCS_PREFIX}/frontend/abstractrigidbodycontroller.h
  ${RBA_SRCS_PREFIX}/frontend/planecontroller.h
  ${RBA_SRCS_PREFIX}/frontend/cylindercontroller.h
  ${RBA_SRCS_PREFIX}/frontend/spherecontroller.h
  ${RBA_SRCS_PREFIX}/frontend/simulatorsettings.h
  ${RBA_SRCS_PREFIX}/frontend/environment.h
  ${RBA_SRCS_PREFIX}/backend/rigidbodyaspect.h

  ${SRCS_PREFIX}/objects/ball.h
  ${SRCS_PREFIX}/objects/tube.h
  ${SRCS_PREFIX}/objects/ground.h

  ${RBA_SRCS_PREFIX}/backend/rigidbodyaspect.h
  )





set( RCCS
  # QML examples
  examples/resources/qml/ex_qml.qrc

# Qt3D KDAB "official" examples
  resources/qt3d_examples/animated_skinned_mesh/qt3d_ex_animated_skinned_mesh.qrc

  resources/qml/qml.qrc
  )

qt5_add_resources( RCCS_MOC ${RCCS} )


# add as external binary resources
qt5_add_binary_resources( ${PROJECT_NAME}_ext_shared_assets_qt3d  DESTINATION external_shared_assets_qt3d.rcc
    resources/shared_assets/ext_shared_assets_qt3d.qrc )



###########################
# Define target: executable
add_executable( ${PROJECT_NAME}
  ${HDRS} 
  ${SRCS} 
  ${HDRS_MOC}
  ${RCCS_MOC} )


add_dependencies(${PROJECT_NAME} ${PROJECT_NAME}_ext_shared_assets_qt3d)


##################
# Compiler options

# CHECK FOR CMAKE C++17 FEATURE CONTROL-COMPATIBILITY
if(${CMAKE_VERSION} VERSION_LESS "3.8.0")
  message(FATAL "${PROJECT_NAME} requires compiling in mode supporting C++17 features.")
endif()

# Features
target_compile_features(${PROJECT_NAME}
  PUBLIC $<$<CXX_COMPILER_ID:Clang>:cxx_std_17>
  PUBLIC $<$<CXX_COMPILER_ID:GNU>:cxx_std_17>
  PUBLIC $<$<CXX_COMPILER_ID:MSVC>:cxx_std_17>
  )

# Definitions
target_compile_definitions( ${PROJECT_NAME}
  PUBLIC $<$<CXX_COMPILER_ID:MSVC>:
    _USE_MATH_DEFINES
    >
  )

# Compiler spesific options
target_compile_options(${PROJECT_NAME}
  PUBLIC $<$<CXX_COMPILER_ID:Clang>:
    -Weverything -pedantic -Werror
    -Wno-c++98-compat -Wno-c++98-compat-pedantic
    -Wno-documentation
    -Wno-padded # Qt
    -Wno-global-constructors
    -Wno-exit-time-destructors
    -Wno-weak-vtables
    -Wno-used-but-marked-unused
    -Wno-missing-prototypes
    -Wno-undefined-reinterpret-cast
    -Wno-unreachable-code
    -Wno-unused-variable
  >
  PUBLIC $<$<CXX_COMPILER_ID:GNU>:
    -pedantic -Wall -Werror
  >
  PUBLIC $<$<CXX_COMPILER_ID:MSVC>:>
    )




find_package(blaze REQUIRED)
if(blaze_FOUND)

  # blaze interface_link_libraries
  get_target_property(blaze_interface_include_directories blaze::blaze INTERFACE_INCLUDE_DIRECTORIES)
  target_include_directories(${PROJECT_NAME} INTERFACE ${blaze_interface_include_directories})

endif()





#########
# Configure linking
target_link_libraries( ${PROJECT_NAME}
  gmlib2::gmlib2qt
  ${QT_TARGET_KEYWORDS}
  blaze::blaze
  ${AUX_BLAZE_LINK_LIBRARIES}
  )


