#ifndef CYLINDERCONTROLLER_H
#define CYLINDERCONTROLLER_H


#include "abstractrigidbodycontroller.h"

namespace rigidbodyaspect
{

  class CylinderController : public AbstractRigidBodyController {
    Q_OBJECT

    Q_PROPERTY(float radius MEMBER m_radius NOTIFY radiusChanged)
    Q_PROPERTY(float height MEMBER m_height NOTIFY heightChanged)

  public:
    CylinderController(Qt3DCore::QNode* parent = nullptr);


    float m_radius{1.0f};
    float m_height{1.0f};

  signals:
    void radiusChanged(float radius);
    void heightChanged(float height);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr createNodeCreationChange() const override;
  };


  struct CylinderInitialData : AbstractRigidBodyInitialData {
    float m_radius;
    float m_height;
  };

}   // namespace rigidbodyaspect

#endif // CYLINDERCONTROLLER_H
