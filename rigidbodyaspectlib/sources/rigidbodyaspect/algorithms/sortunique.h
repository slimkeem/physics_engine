#ifndef SORTUNIQUE_H
#define SORTUNIQUE_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <set>
#include <functional>

#include "cacheproperty.h"
#include "collisiondetection.h"
//#include "../jobs/genericcolliderjob.h"


namespace rigidbodyaspect::algorithms::sortunique
{

  using UniqueSphereSet = std::set<rbtypes::Sphere*>;

  void driveSortUnique (collisiondetection::detail::SpherePlaneCollisionContainer  &sphere_plane_collisions,
                        collisiondetection::detail::SphereSphereCollisionContainer &sphere_sphere_collisions);




}   // namespace rigidbodyaspect::algorithms::sortunique


#endif // SORTUNIQUE_H
