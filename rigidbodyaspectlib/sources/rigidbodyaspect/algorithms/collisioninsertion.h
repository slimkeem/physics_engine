#ifndef COLLISIONINSERTION_H
#define COLLISIONINSERTION_H

#include "collisiondetection.h"

#include "../backend/rigidbodyaspect.h"

namespace rigidbodyaspect::algorithms::collisioninsertion
{

using SpherePlaneCollisionObject = std::tuple<rbtypes::Sphere*, rbtypes::FixedPlane*, seconds_type>;
using SphereSphereCollisionObject = std::tuple<rbtypes::Sphere*, rbtypes::Sphere*, seconds_type>;
using SpherePlaneCollisionContainer = std::vector<SpherePlaneCollisionObject>;
using SphereSphereCollisionContainer = std::vector<SphereSphereCollisionObject>;

void insertNewSpherePlaneCollisions (SpherePlaneCollisionContainer &sp_pl_col,
                                     SpherePlaneCollisionObject    &sp_pl_col_latest,
                                     RigidBodyAspect               *m_aspect,
                                     seconds_type                  m_dt);

void insertNewSphereSphereCollisions (SphereSphereCollisionContainer &sp_sp_col,
                                      SphereSphereCollisionObject    &sp_sp_col_latest,
                                      RigidBodyAspect                *m_aspect,
                                      seconds_type                   m_dt);

}


#endif // INNERLOOP_H
