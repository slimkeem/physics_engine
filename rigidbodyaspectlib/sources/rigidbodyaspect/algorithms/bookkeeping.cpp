#include "bookkeeping.h"

namespace rigidbodyaspect::algorithms::bookkeeping
{


  void addAttachments(rbtypes::Sphere                          &sphere,
                      rigidbodyaspect::detail::attachment_pair &attachment)
  {
    auto attachment_type = attachment.second;
    if (attachment_type == detail::attachment_types::plane or
        attachment_type == detail::attachment_types::cylinder){
      if(sphere.m_attachments.empty())
        sphere.m_attachments.emplace_back(attachment);
      for (auto &old_attachment : sphere.m_attachments){
        if (&attachment == &old_attachment)
          continue;
        sphere.m_attachments.emplace_back(attachment);
      }
      std::cout<< "emplacing attachment: " << attachment.first <<std::endl;
    }

    else if (attachment_type == detail::attachment_types::sphere){
      auto attached_sphere = static_cast<rbtypes::Sphere*>(attachment.first);
      if (!sphere.m_attachments.empty()){
        for (auto &attachment : sphere.m_attachments){
          if (attachment.first == &attached_sphere)
            continue;
          sphere.m_attachments.emplace_back(attachment);
          std::cout<< "emplacing attachment: " << attachment.first <<std::endl;
        }
      }
      sphere.m_attachments.emplace_back(attachment);
      std::cout<< "emplacing attachment: " << attachment.first <<std::endl;

      if (!sphere.m_attachments.empty()){
        for (auto &attachment : attached_sphere->m_attachments){
          if (attachment.first == &sphere)
            continue;
          attached_sphere->m_attachments.emplace_back(attachment);
          std::cout<< "emplacing attachment: " << attachment.first <<std::endl;
        }
      }
      attached_sphere->m_attachments.emplace_back(attachment);
      std::cout<< "emplacing attachment: " << attachment.first <<std::endl;
    }
  }


  void clearAttachments(rbtypes::Sphere &sphere)
  {
    for (auto &attachment : sphere.m_attachments){
      auto attachment_type = attachment.second;
      if (attachment_type == detail::attachment_types::plane or
          attachment_type == detail::attachment_types::cylinder){
        sphere.m_attachments.pop_back();
        std::cout<< "Clearing attachments" <<std::endl;
      }

      else if (attachment_type == detail::attachment_types::sphere){
        auto attached_sphere = static_cast<rbtypes::Sphere*>(attachment.first);
        attached_sphere->m_attachments.pop_back();
        std::cout<< "Clearing attachments" <<std::endl;
        sphere.m_attachments.pop_back();
        std::cout<< "Clearing attachments" <<std::endl;
      }
    }
  }
}
