#include "collisioninsertion.h"

namespace rigidbodyaspect::algorithms::collisioninsertion
{

  void insertNewSpherePlaneCollisions(SpherePlaneCollisionContainer &sp_pl_col,
                                      SpherePlaneCollisionObject    &sp_pl_col_latest,
                                      RigidBodyAspect               *m_aspect,
                                      seconds_type                  m_dt)
  {
    auto& rigid_bodies = m_aspect->rigidBodies();
    for (auto sphere1_it = rigid_bodies.spheres().begin();
         sphere1_it != rigid_bodies.spheres().end(); ++sphere1_it) {
      const auto dt_rem = m_dt - sphere1_it->m_dt_tp;
      for (auto& fixed_plane : rigid_bodies.fixedPlanes()) {
        const auto collision_check = algorithms::collisiondetection::detectCollisionSphereFixedplane
            (*sphere1_it, fixed_plane, dt_rem).second;
        const auto impact_time =     algorithms::collisiondetection::detectCollisionSphereFixedplane
            (*sphere1_it, fixed_plane, dt_rem).first;
        if (&fixed_plane == &*std::get<1>(sp_pl_col_latest))
          continue;
        if (collision_check == algorithms::collisiondetection::detail::ColliderStatus::Collision){
          if(sphere1_it->m_attachments.empty())
            sp_pl_col.emplace_back(&*sphere1_it, &fixed_plane, impact_time);
          for (auto &attachment : sphere1_it->m_attachments){
            auto attachment_type = attachment.second;
            if (attachment_type == detail::attachment_types::plane){
              auto attached_plane = static_cast<rbtypes::FixedPlane*>(attachment.first);
              if (&fixed_plane == attached_plane)
                continue;
              sp_pl_col.emplace_back(&*sphere1_it, &fixed_plane, impact_time);
            }
          }
        }
      }
    }
  }

  void insertNewSphereSphereCollisions(SphereSphereCollisionContainer &sp_sp_col,
                                       SphereSphereCollisionObject    &sp_sp_col_latest,
                                       RigidBodyAspect                *m_aspect,
                                       seconds_type                   m_dt)
  {
    auto& rigid_bodies = m_aspect->rigidBodies();
    for (auto sphere1_it = rigid_bodies.spheres().begin();
         sphere1_it != rigid_bodies.spheres().end(); ++sphere1_it) {
      for (auto sphere2_it  = sphere1_it+1; sphere2_it != rigid_bodies.spheres().end(); ++sphere2_it){
        auto max_dt_tp = std::max(sphere1_it->m_dt_tp,sphere2_it->m_dt_tp);
        const auto dt_rem_max = m_dt - max_dt_tp;
        const auto collision_check = algorithms::collisiondetection::detectCollisionSphereSphere
            (*sphere1_it, *sphere2_it, dt_rem_max).second;
        const auto impact_time =     algorithms::collisiondetection::detectCollisionSphereSphere
            (*sphere1_it, *sphere2_it, dt_rem_max).first;
        if (&*sphere2_it == &(*std::get<1>(sp_sp_col_latest)))
          continue;
        if (collision_check == algorithms::collisiondetection::detail::ColliderStatus::Collision){
          if(sphere1_it->m_attachments.empty())
            sp_sp_col.emplace_back(&*sphere1_it, &*sphere2_it, impact_time);
          for (auto &attachment : sphere1_it->m_attachments){
            auto attachment_type = attachment.second;
            if (attachment_type == detail::attachment_types::sphere){
              auto attached_sphere = static_cast<rbtypes::Sphere*>(attachment.first);
              if (&*sphere2_it == attached_sphere)
                continue;
              sp_sp_col.emplace_back(&*sphere1_it, &*sphere2_it, impact_time);
            }
          }
          sp_sp_col.emplace_back(&*sphere1_it, &*sphere2_it, impact_time);
        }
      }
    }
  }

}
