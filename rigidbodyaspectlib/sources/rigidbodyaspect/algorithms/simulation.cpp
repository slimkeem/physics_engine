#include "simulation.h"
#include "collisiondetection.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(MovingBody& body,
                                 double      sub_dt_factor)
  {

    body.translateParent(body.m_ds * sub_dt_factor);

    body.m_velocity += body.m_acceleration * sub_dt_factor;
    if (body.m_state == rigidbodyaspect::detail::States::Sliding){
      for (auto &attachment : body.m_attachments){
        auto attachment_type = attachment.second;
        if (attachment_type == detail::attachment_types::plane){
          auto sphere = static_cast<rbtypes::Sphere*>(attachment.first);
          auto attached_plane = static_cast<rbtypes::FixedPlane*>(attachment.first);
          auto n_d_pair = closestpoint::plane_displacement_normal(*sphere, *attached_plane);
          auto n = n_d_pair.first;
          body.m_velocity = (body.m_velocity - (2 * (blaze::inner(body.m_velocity,n)*n))*0.95);
        }
      }
    }

  }

}   // namespace rigidbodyaspect::algorithms::dynamics
