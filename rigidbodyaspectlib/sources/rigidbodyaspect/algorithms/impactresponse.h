#ifndef IMPACTRESPONSE_H
#define IMPACTRESPONSE_H

#include "../types.h"
#include "../geometry/rbtypes.h"


namespace rigidbodyaspect::algorithms::impactresponse
{

  void computeSpherePlaneImpactResponse (rbtypes::Sphere     &sphere,
                                         rbtypes::FixedPlane &plane);

  void computeSphereSphereImpactResponse (rbtypes::Sphere &sphere1,
                                          rbtypes::Sphere &sphere2);

  void computeSphereCylinderImpactResponse (rbtypes::Sphere        &sphere1,
                                            rbtypes::FixedCylinder &cylinder);

}   // namespace rigidbodyaspect::algorithms::impactresponse


#endif // IMPACTRESPONSE_H
