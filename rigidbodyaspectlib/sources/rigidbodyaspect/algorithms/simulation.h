#ifndef RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
#define RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H

#include "../geometry/movingbody.h"
#include "../backend/environmentbackend.h"
#include "../backend/rigidbodyaspect.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(MovingBody& body,
                                 double      sub_dt_factor);

}   // namespace rigidbodyaspect::algorithms::simulation


#endif   // RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
