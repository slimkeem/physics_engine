#ifndef STATECHANGE_H
#define STATECHANGE_H

#include "../backend/rigidbodyaspect.h"

// stl
#include <variant>

namespace rigidbodyaspect::algorithms::statechange
{

  namespace detail
  {
    enum class StateChangeStatus { ToAtRest, ToSliding, ToFree, NoChange };
    using      n_q_pair          = std::pair<GM2Vector, GM2Vector>;

  }   // namespace detail

  //With Plane
  void resolveSpPlStateChange(rbtypes::Sphere     &sphere,
                              rbtypes::FixedPlane &plane);

  detail::StateChangeStatus
  checkSpPlStateChange(rbtypes::Sphere     &sphere,
                       rbtypes::FixedPlane &plane);


  detail::n_q_pair
  Pl_n_q_pair(rbtypes::FixedPlane &plane);


  //With Sphere
  void resolveSpSpStateChange(rbtypes::Sphere &sphere1,
                              rbtypes::Sphere &sphere2);

  detail::StateChangeStatus
  checkSpSpStateChange(rbtypes::Sphere &sphere1,
                       rbtypes::Sphere &sphere2);


  detail::n_q_pair
  Sp_n_q_pair(rbtypes::Sphere &sphere);

}  //namespace rigidbodyaspect::algorithms::statechange

#endif // STATECHANGE_H
