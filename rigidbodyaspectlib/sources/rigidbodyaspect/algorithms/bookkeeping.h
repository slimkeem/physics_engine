#ifndef BOOKKEEPING_H
#define BOOKKEEPING_H

#include "../geometry/rbtypes.h"


namespace rigidbodyaspect::algorithms::bookkeeping
{

  void clearAttachments(rbtypes::Sphere &sphere);

  void addAttachments(rbtypes::Sphere                          &sphere,
                      rigidbodyaspect::detail::attachment_pair &attachment);

} //namespace rigidbodyaspect::algorithms::bookkeeping

#endif // BOOKKEEPING_H
