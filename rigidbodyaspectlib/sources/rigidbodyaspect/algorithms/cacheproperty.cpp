#include "cacheproperty.h"
#include "bookkeeping.h"
#include "collisiondetection.h"
#include "../backend/environmentbackend.h"

namespace rigidbodyaspect::algorithms::cacheproperty
{

  void
  calcProperties(rbtypes::Sphere         &sphere,
                 RigidBodyAspect         *m_aspect,
                 seconds_type            dt)
  {

    const auto env_id     = sphere.m_env_id;
    const auto env_BE     = m_aspect->environmentBackend(env_id);
    const auto Gn         = env_BE ? env_BE->m_gravity : GM2Vector{0, 20.0, 0};
    const auto forces     = Gn;
    sphere.m_acceleration = forces * dt.count();
    sphere.m_ds           = (sphere.m_velocity + 0.5*sphere.m_acceleration)*dt.count();

    //computational models
    if (sphere.m_state == rigidbodyaspect::detail::States::AtRest)
      sphere.m_ds = 0;

    else if (sphere.m_state == rigidbodyaspect::detail::States::Sliding){
      auto n    = GM2Vector{0,0,0};
      auto d    = GM2Vector{0,0,0};
      auto n_cs = GM2Vector{0,0,0};
      for (auto &attachment : sphere.m_attachments){
        auto attachment_type = attachment.second;
        if (attachment_type == detail::attachment_types::plane){
          auto attached_plane = static_cast<rbtypes::FixedPlane*>(attachment.first);
          auto n_d_pair = closestpoint::plane_displacement_normal(sphere, *attached_plane);
          n = n_d_pair.first;
          d = n_d_pair.second;
          n_cs += n;

          auto uv_pair = closestpoint::calcPlaneClosestPoint(sphere, *attached_plane);
          if (uv_pair[0] < attached_plane->startParameters().at(0) or
              uv_pair[0] < attached_plane->startParameters().at(1) or
              uv_pair[1] > attached_plane->endParameters().at(0) or
              uv_pair[1] > attached_plane->endParameters().at(1)){
            bookkeeping::clearAttachments(sphere);
            sphere.m_state = rigidbodyaspect::detail::States::Free;
          }


        }
      }
      if (blaze::inner(sphere.m_ds,n_cs)<0)
        sphere.m_ds += d;
    }
  }
}



