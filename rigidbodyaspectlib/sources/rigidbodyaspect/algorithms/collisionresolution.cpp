#include "collisionresolution.h"

#include "collisiondetection.h"
#include "impactresponse.h"
#include "simulation.h"
#include "cacheproperty.h"
#include "../jobs/genericcolliderjob.h"
#include "collisioninsertion.h"
#include "statechange.h"
#include "collisiondetection.h"

namespace rigidbodyaspect::algorithms::collisionresolution
{

  void resolveSpherePlaneCollision(SpherePlaneCollisionContainer &sp_pl_col,
                                   RigidBodyAspect               *m_aspect,
                                   seconds_type                  m_dt)
  {

    auto sp_pl_col_latest                = sp_pl_col.back();
    sp_pl_col.pop_back();
    const auto& sp_pl_col_sphere_address = std::get<0>(sp_pl_col_latest);
    const auto& sp_pl_col_plane_address  = std::get<1>(sp_pl_col_latest);
    const auto& sp_pl_col_tp             = std::get<2>(sp_pl_col_latest);
    auto  &sp_pl_col_sphere              = *sp_pl_col_sphere_address;
    auto& sp_pl_col_plane                = *sp_pl_col_plane_address;


    sp_pl_col_sphere.m_dt_tp  += sp_pl_col_tp;


    // 4A.1 Simulate Moving Objects
    auto dt_rem           = m_dt - sp_pl_col_sphere.m_dt_tp;
    simulation::simulateGenericMovingBody(sp_pl_col_sphere, sp_pl_col_sphere.m_dt_tp/m_dt);

    // 4A.2 Compute Impact Response
    impactresponse::computeSpherePlaneImpactResponse(sp_pl_col_sphere, sp_pl_col_plane);

    // 4A.3 Compute Updated Cache Properties
    cacheproperty::calcProperties(sp_pl_col_sphere, m_aspect, dt_rem);

    // 4A.4 Detect state changes
    statechange::resolveSpPlStateChange(sp_pl_col_sphere, sp_pl_col_plane);

    // 4A.5 Recompute cache props again
    cacheproperty::calcProperties(sp_pl_col_sphere, m_aspect, dt_rem);

    // 4A.6 Recheck for new collisions
    collisioninsertion::insertNewSpherePlaneCollisions
        (sp_pl_col, sp_pl_col_latest, m_aspect, m_dt);

  }


  void resolveSphereSphereCollision(SphereSphereCollisionContainer &sp_sp_col,
                                    RigidBodyAspect                *m_aspect,
                                    seconds_type                   m_dt)
  {
    auto sp_sp_col_latest                 = sp_sp_col.back();
    sp_sp_col.pop_back();
    const auto& sp_sp_col_sphere_address  = std::get<0>(sp_sp_col_latest);
    const auto& sp_sp_col_sphere2_address = std::get<1>(sp_sp_col_latest);
    const auto& sp_sp_col_tp              = std::get<2>(sp_sp_col_latest);
    auto& sp_sp_col_sphere                = *sp_sp_col_sphere_address;
    auto& sp_sp_col_sphere2               = *sp_sp_col_sphere2_address;


    sp_sp_col_sphere.m_dt_tp  += sp_sp_col_tp;
    sp_sp_col_sphere2.m_dt_tp  += sp_sp_col_tp;


    // 4B.1.1 Simulate 1st Moving Object
    auto dt_rem1        = m_dt - sp_sp_col_sphere.m_dt_tp;
    simulation::simulateGenericMovingBody(sp_sp_col_sphere, sp_sp_col_sphere.m_dt_tp/m_dt);

    // 4B.1.2 Simulate 2nd Moving Object
    auto dt_rem2         = m_dt - sp_sp_col_sphere2.m_dt_tp;
    simulation::simulateGenericMovingBody(sp_sp_col_sphere2, sp_sp_col_sphere2.m_dt_tp/m_dt);


    // 4B.2 Compute Impact Response
    impactresponse::computeSphereSphereImpactResponse(sp_sp_col_sphere, sp_sp_col_sphere2);


    // 4B.3.1 Compute Updated Cache Property of 1st moving object
    cacheproperty::calcProperties(sp_sp_col_sphere, m_aspect, dt_rem1);

    // 4B.3.2 Compute Updated Cache Property of 2nd moving object
    cacheproperty::calcProperties(sp_sp_col_sphere2, m_aspect, dt_rem2);


    // 4B.4 Detect state changes
    statechange::resolveSpSpStateChange(sp_sp_col_sphere, sp_sp_col_sphere2);
    statechange::resolveSpSpStateChange(sp_sp_col_sphere2, sp_sp_col_sphere);


    // 4B.5 Recompute cache props again
    algorithms::cacheproperty::calcProperties(sp_sp_col_sphere, m_aspect, dt_rem1);
    algorithms::cacheproperty::calcProperties(sp_sp_col_sphere2, m_aspect, dt_rem2);


    // 4B.6 Recheck for new collisions
    algorithms::collisioninsertion::insertNewSphereSphereCollisions
        (sp_sp_col,sp_sp_col_latest,m_aspect,m_dt);

  }




}
