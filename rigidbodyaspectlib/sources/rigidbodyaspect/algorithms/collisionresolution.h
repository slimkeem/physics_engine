#ifndef COLLISIONRESOLUTION_H
#define COLLISIONRESOLUTION_H

#include "../geometry/rbtypes.h"

#include "../backend/rigidbodyaspect.h"
namespace rigidbodyaspect::algorithms::collisionresolution
{

using SpherePlaneCollisionObject      = std::tuple<rbtypes::Sphere*, rbtypes::FixedPlane*, seconds_type>;
using SphereSphereCollisionObject     = std::tuple<rbtypes::Sphere*, rbtypes::Sphere*, seconds_type>;
using SpherePlaneCollisionContainer   = std::vector<SpherePlaneCollisionObject>;
using SphereSphereCollisionContainer  = std::vector<SphereSphereCollisionObject>;

void resolveSpherePlaneCollision (SpherePlaneCollisionContainer &sp_pl_col,
                                  RigidBodyAspect               *m_aspect,
                                  seconds_type                  m_dt);

void resolveSphereSphereCollision (SphereSphereCollisionContainer &sp_sp_col,
                                   RigidBodyAspect                *m_aspect,
                                   seconds_type                   m_dt);

}


#endif // COLLISIONRESOLUTION_H
