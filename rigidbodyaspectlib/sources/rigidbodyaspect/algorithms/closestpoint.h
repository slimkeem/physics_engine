#ifndef CLOSESTPOINT_H
#define CLOSESTPOINT_H

#include "../geometry/rbtypes.h"
#include "../backend/rigidbodyaspect.h"


namespace rigidbodyaspect::algorithms::closestpoint
{

  namespace detail
  {
    using      Vector2UL = blaze::StaticVector<double, 2UL, blaze::columnVector>;
    using      n_d_pair  = std::pair<GM2Vector, GM2Vector>;
  }

  //Plane
  detail::Vector2UL calcPlaneClosestPoint(rbtypes::Sphere      &sphere,
                                          rbtypes::FixedPlane  &plane);

  detail::n_d_pair plane_displacement_normal(rbtypes::Sphere      &sphere,
                                             rbtypes::FixedPlane  &plane);

  detail::Vector2UL calcCylinderClosestPoint(rbtypes::Sphere      &sphere,
                                          rbtypes::FixedCylinder  &cylinder);

  detail::n_d_pair cylinder_displacement_normal(rbtypes::Sphere      &sphere,
                                             rbtypes::FixedCylinder  &cylinder);

}

#endif // CLOSESTPOINT_H
