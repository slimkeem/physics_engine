#ifndef CACHEPROPERTY_H
#define CACHEPROPERTY_H

#include "../geometry/rbtypes.h"
#include "../backend/rigidbodyaspect.h"
#include "closestpoint.h"

namespace rigidbodyaspect::algorithms::cacheproperty
{


  void calcProperties (rbtypes::Sphere                           &sphere,
                       RigidBodyAspect                           *m_aspect,
                       seconds_type                              m_dt/*,
                       rigidbodyaspect::detail::attachment_pair  &attachment*/);

}   // namespace rigidbodyaspect::algorithms::cacheproperty


#endif // CACHEPROPERTY_H
