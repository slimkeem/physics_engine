#include "closestpoint.h"

namespace rigidbodyaspect::algorithms::closestpoint
{

  detail::Vector2UL calcPlaneClosestPoint(rbtypes::Sphere     &sphere,
                                          rbtypes::FixedPlane &plane)
  {
    auto u            = 0.0;
    auto v            = 0.0;
    const auto p_old  = sphere.frameOriginParent();
    const auto ds     = sphere.m_ds;
    const auto p      = ds + p_old;
    detail::Vector2UL uv_pair;
    auto du           = 1.0;
    auto dv           = 1.0;
    blaze::DynamicMatrix<blaze::StaticVector<double,4,0>,0> surface_eval;
    blaze::DynamicMatrix<double> A(2UL, 2UL);

    while (du > DBL_EPSILON ,dv > DBL_EPSILON){
      surface_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{u, v},
                                          rbtypes::FixedPlane::PSizeArray{1UL, 1UL});
      const auto q      = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 0UL));
      const auto s_u    = blaze::subvector<0UL, 3UL>(surface_eval(1UL, 0UL));
      const auto s_v    = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 1UL));
      //        const auto s_uu   = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 1UL));
      //        const auto s_vv   = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 1UL));
      //        const auto s_uv   = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 1UL));
      const auto d      = q - p;

      A = {
            {/*blaze::inner(d,s_uu) +*/ blaze::inner(s_u,s_u),/*blaze::inner(d,s_uv) + */blaze::inner(s_u,s_v)},
            {/*blaze::inner(d,s_uv) +*/ blaze::inner(s_u,s_v),/*blaze::inner(d,s_vv) +*/ blaze::inner(s_v,s_v)}
          };

      blaze::DynamicVector<double, blaze::columnVector> b (2UL);

      b = {-blaze::inner(d,s_u),-blaze::inner(d,s_v)};

      blaze::DynamicVector<double, blaze::columnVector> x = blaze::inv(A)*b;
      du = x[0];
      dv = x[1];

      uv_pair += x;
      u = uv_pair[0];
      v = uv_pair[1];
    }

    return uv_pair;

  }

  detail::n_d_pair plane_displacement_normal(rbtypes::Sphere &sphere,
                                             rbtypes::FixedPlane &plane)
  {
    const auto uv_pair = calcPlaneClosestPoint(sphere, plane);
    const auto u      = uv_pair[0];
    const auto v      = uv_pair[1];
    const auto p_old  = sphere.frameOriginParent();
    const auto ds     = sphere.m_ds;
    const auto p      = ds + p_old;
    const auto r      = sphere.m_radius;
    auto  n      = GM2Vector{0,0,0};
    auto  d      = GM2Vector{0,0,0};

    auto surface_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{u, v},
                                              rbtypes::FixedPlane::PSizeArray{1UL, 1UL});
    const auto q    = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 0UL));
    const auto pl_u = blaze::subvector<0UL, 3UL>(surface_eval(1UL, 0UL));
    const auto pl_v = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 1UL));
    n = blaze::normalize(blaze::cross(pl_u, pl_v));
    d = (q-p)+(n*r);

    return detail::n_d_pair{n, d};

  }

  detail::Vector2UL calcCylinderClosestPoint(rbtypes::Sphere &sphere,
                                             rbtypes::FixedCylinder &cylinder)
  {
    auto u            = 0.0;
    auto v            = 0.0;
    const auto p_old  = sphere.frameOriginParent();
    const auto ds     = sphere.m_ds;
    const auto p      = ds + p_old;
    detail::Vector2UL uv_pair;
    auto du           = 1.0;
    auto dv           = 1.0;
    blaze::DynamicMatrix<blaze::StaticVector<double,4,0>,0> surface_eval;
    blaze::DynamicMatrix<double> A(2UL, 2UL);

    while (du > DBL_EPSILON ,dv > DBL_EPSILON){
      surface_eval = cylinder.evaluateParent(rbtypes::FixedCylinder::PSpacePoint{u, v},
                                          rbtypes::FixedCylinder::PSizeArray{1UL, 1UL});
      const auto q      = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 0UL));
      const auto s_u    = blaze::subvector<0UL, 3UL>(surface_eval(1UL, 0UL));
      const auto s_v    = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 1UL));
      //        const auto s_uu   = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 1UL));
      //        const auto s_vv   = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 1UL));
      //        const auto s_uv   = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 1UL));
      const auto d      = q - p;

      A = {
            {/*blaze::inner(d,s_uu) +*/ blaze::inner(s_u,s_u),/*blaze::inner(d,s_uv) + */blaze::inner(s_u,s_v)},
            {/*blaze::inner(d,s_uv) +*/ blaze::inner(s_u,s_v),/*blaze::inner(d,s_vv) +*/ blaze::inner(s_v,s_v)}
          };

      blaze::DynamicVector<double, blaze::columnVector> b (2UL);

      b = {-blaze::inner(d,s_u),-blaze::inner(d,s_v)};

      blaze::DynamicVector<double, blaze::columnVector> x = blaze::inv(A)*b;
      du = x[0];
      dv = x[1];

      uv_pair += x;
      u = uv_pair[0];
      v = uv_pair[1];
    }

    return uv_pair;
  }

  detail::n_d_pair cylinder_displacement_normal(rbtypes::Sphere &sphere, rbtypes::FixedCylinder &cylinder)
  {
    const auto uv_pair = calcCylinderClosestPoint(sphere, cylinder);
    const auto u      = uv_pair[0];
    const auto v      = uv_pair[1];
    const auto p_old  = sphere.frameOriginParent();
    const auto ds     = sphere.m_ds;
    const auto p      = ds + p_old;
    const auto r      = sphere.m_radius;
    auto  n      = GM2Vector{0,0,0};
    auto  d      = GM2Vector{0,0,0};

    auto surface_eval = cylinder.evaluateParent(rbtypes::FixedCylinder::PSpacePoint{u, v},
                                                rbtypes::FixedCylinder::PSizeArray{1UL, 1UL});
    const auto q    = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 0UL));
    const auto pl_u = blaze::subvector<0UL, 3UL>(surface_eval(1UL, 0UL));
    const auto pl_v = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 1UL));
    n = blaze::normalize(blaze::cross(pl_u, pl_v));
    d = (q-p)+(n*r);

    return detail::n_d_pair{n, d};

  }


}
