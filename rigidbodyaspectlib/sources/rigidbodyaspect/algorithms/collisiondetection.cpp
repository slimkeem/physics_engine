#include "collisiondetection.h"
#include "simulation.h"
#include "closestpoint.h"

namespace rigidbodyaspect::algorithms::collisiondetection
{

  detail::CollisionCheck
  detectCollisionSphereFixedplane(rbtypes::Sphere     &sphere,
                                  rbtypes::FixedPlane &plane,
                                  seconds_type        dt)
  {
    blaze::DynamicMatrix<blaze::StaticVector<double,4,0>,0> pl_eval;
    pl_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                                   rbtypes::FixedPlane::PSizeArray{1UL, 1UL});

    const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
    const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
    const auto q = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));
    const auto n = blaze::normalize(blaze::cross(pl_u, pl_v));

    const auto p = sphere.frameOriginParent();
    const auto r = sphere.m_radius;
    const auto ds = sphere.m_ds;

    const auto d = (q + (r * n)) - p;

    const auto R = blaze::inner(sphere.m_ds,n);
    const auto Q = blaze::inner(d,n);

    auto uv_pair = closestpoint::calcPlaneClosestPoint(sphere, plane);

    if (std::abs(R) < DBL_EPSILON){
      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
    }
    else if (std::abs(Q) < DBL_EPSILON){
      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
    }

    else{
      const auto x = Q/R;
      const auto dt_tp = x * dt;
      if (x > 0.0 && x <= 1.0){
        if (uv_pair[0] < plane.startParameters().at(0) or
            uv_pair[0] < plane.startParameters().at(1) or
            uv_pair[1] > plane.endParameters().at(0) or
            uv_pair[1] > plane.endParameters().at(1))
          return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
        return detail::CollisionCheck{dt_tp, detail::ColliderStatus::Collision};
      }
      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
    }

  }


  detail::CollisionCheck
  detectCollisionSphereSphere(rbtypes::Sphere &sphere1,
                              rbtypes::Sphere &sphere2,
                              seconds_type    dt)
  {
    auto max_spheres_dt_tp = std::max(sphere2.m_dt_tp,sphere1.m_dt_tp);
    auto min_spheres_dt_tp = std::min(sphere2.m_dt_tp,sphere1.m_dt_tp);
    const auto rel_dt_tp   = max_spheres_dt_tp - min_spheres_dt_tp;
    const auto rel_dt_tp_factor = rel_dt_tp / dt;

    const auto p1  = sphere1.frameOriginParent();
    const auto r1  = sphere1.m_radius;

    const auto p2  = sphere2.frameOriginParent();
    const auto r2  = sphere2.m_radius;

    auto ds1 = sphere1.m_ds;
    auto ds2 = sphere2.m_ds;

    if (sphere1.m_dt_tp != sphere2.m_dt_tp){
      if (sphere1.m_dt_tp < max_spheres_dt_tp)
        ds1 *= rel_dt_tp_factor;
      ds2 *= rel_dt_tp_factor;
    }

    const auto R = (ds2 - ds1);

    const auto Q = p2 - p1;
    const auto r = r1 + r2;

    if (std::abs(blaze::inner(R,R)) < DBL_EPSILON or
        std::abs(pow(blaze::inner(Q,R),2)-
                 (blaze::inner(R,R)*(blaze::inner(Q,Q)-pow(r,2)))) < DBL_EPSILON)
      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
    else if (std::abs(blaze::inner(Q,Q)-pow(r,2)) < DBL_EPSILON)
      //return noCol and switch to sliding
      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
    else {
      const auto x = ((-(blaze::inner(Q,R))) -
                      (std::sqrt(pow(blaze::inner(Q,R),2)-
                                 (blaze::inner(R,R)*(blaze::inner(Q,Q)-pow(r,2)))))) /
          blaze::inner(R,R);
      const auto xdt = x * dt;

      if (x > 0.0 && x <= 1.0)
        return detail::CollisionCheck{xdt, detail::ColliderStatus::Collision};
      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
    }

  }

//  detail::CollisionCheck
//  detectCollisionSphereCylinder(rbtypes::Sphere        &sphere,
//                                rbtypes::FixedCylinder &cylinder,
//                                seconds_type           dt)
//  {
//    const auto p = sphere.frameOriginParent();
//    const auto r_p = sphere.m_radius;

//    blaze::DynamicMatrix<blaze::StaticVector<double,4,0>,0> pl_eval;
//    pl_eval = cylinder.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
//                                      rbtypes::FixedPlane::PSizeArray{1UL, 1UL});
//    const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
//    const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));

//    const auto q   = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));
//    const auto r_q = cylinder.m_radius;

//    const auto vec         = blaze::normalize(blaze::cross(pl_u, pl_v));
//    auto vec_p             = sphere.frameOriginParent();
//    auto vec_d             = (q + vec*r_q) - vec_p;
//    auto vec_c             = blaze::normalize(blaze::cross(q, pl_v));
//    auto vec_c_new         = blaze::cross(vec_c, vec_d);

//    auto n             = blaze::cross(vec_c_new, vec_c)/blaze::length(blaze::cross(vec_c_new, vec_c));
//    auto d = (q + (r_p + r_q)*n)-p;

//    const auto R = blaze::inner(sphere.m_ds,n);
//    const auto Q = blaze::inner(d,n);

//    auto uv_pair = closestpoint::calcCylinderClosestPoint(sphere, cylinder);

//    if (std::abs(R) < DBL_EPSILON){
//      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
//    }
//    else if (std::abs(Q) < DBL_EPSILON){
//      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
//    }

//    else{
//      const auto x = Q/R;
//      const auto dt_tp = x * dt;
//      if (x > 0.0 && x <= 1.0){
//        std::cout << "baba ooo" <<std::endl;
//        if (uv_pair[0] < cylinder.startParameters().at(0) or
//            uv_pair[0] < cylinder.startParameters().at(1) or
//            uv_pair[1] > cylinder.endParameters().at(0) or
//            uv_pair[1] > cylinder.endParameters().at(1))
//          return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
//        return detail::CollisionCheck{dt_tp, detail::ColliderStatus::Collision};
//      }
//      return detail::CollisionCheck{0, detail::ColliderStatus::NoCollision};
//    }

//  }

}   // namespace rigidbodyaspect::algorithms::collisiondetection
