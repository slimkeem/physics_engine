#ifndef RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
#define RIGIDBODYASPECT_ALGORITHMS_COLLISION_H

#include "../types.h"
#include "../geometry/rbtypes.h"
#include "../geometry/rigidbodycontainer.h"
#include "cacheproperty.h"
#include "../backend/rigidbodyaspect.h"

#include "statechange.h"

// stl
#include <variant>


namespace rigidbodyaspect::algorithms::collisiondetection
{

namespace detail
{
enum class ColliderStatus { NoCollision, Collision };
using CollisionCheck  = std::pair<seconds_type, ColliderStatus>;

using SpherePlaneCollisionObject = std::tuple<rbtypes::Sphere*, rbtypes::FixedPlane*, seconds_type>;
using SphereSphereCollisionObject = std::tuple<rbtypes::Sphere*, rbtypes::Sphere*, seconds_type>;
using SphereCylinderCollisionObject = std::tuple<rbtypes::Sphere*, rbtypes::FixedCylinder*, seconds_type>;

using SpherePlaneCollisionContainer = std::vector<SpherePlaneCollisionObject>;
using SphereSphereCollisionContainer = std::vector<SphereSphereCollisionObject>;
using SphereCylinderCollisionContainer = std::vector<SphereCylinderCollisionObject>;
}   // namespace detail

detail::CollisionCheck
detectCollisionSphereFixedplane(rbtypes::Sphere     &sphere,
                                rbtypes::FixedPlane &plane,
                                seconds_type        dt);

detail::CollisionCheck
detectCollisionSphereSphere(rbtypes::Sphere &sphere1,
                            rbtypes::Sphere &sphere2,
                            seconds_type    dt);

//detail::CollisionCheck
//detectCollisionSphereCylinder(rbtypes::Sphere        &sphere,
//                              rbtypes::FixedCylinder &cylinder,
//                              seconds_type           dt);

}   // namespace rigidbodyaspect::algorithms::collisiondetection


#endif   // RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
