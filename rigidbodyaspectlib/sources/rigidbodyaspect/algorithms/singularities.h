#ifndef SINGULARITIES_H
#define SINGULARITIES_H

#include "../types.h"
#include "../geometry/rbtypes.h"


namespace rigidbodyaspect::algorithms::singularities
{

  void detectSingularities(rbtypes::Sphere&     sphere);

}   // namespace rigidbodyaspect::algorithms::singularities

#endif // SINGULARITIES_H
