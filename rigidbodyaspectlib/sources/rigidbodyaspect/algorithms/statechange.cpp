#include "statechange.h"

#include "cacheproperty.h"
#include "collisiondetection.h"
#include "bookkeeping.h"
#include "closestpoint.h"

namespace rigidbodyaspect::algorithms::statechange
{

  void resolveSpPlStateChange(rbtypes::Sphere     &sphere,
                              rbtypes::FixedPlane &plane)
  {
    detail::StateChangeStatus status = checkSpPlStateChange(sphere, plane);

    //Attachment definitions
    rigidbodyaspect::detail::attachment_pair
        plane_attachment(&plane, rigidbodyaspect::detail::attachment_types::plane);

    //State resolution
    if (status == statechange::detail::StateChangeStatus::ToAtRest){
      bookkeeping::addAttachments(sphere, plane_attachment);
      sphere.m_state = rigidbodyaspect::detail::States::AtRest;
      std::cout << "to resting in peace!" << std::endl;
    }

    else if (status == statechange::detail::StateChangeStatus::ToSliding) {
      //auto n_d_pair = closestpoint::closestPointDistNorm(sphere, plane);
      bookkeeping::addAttachments(sphere, plane_attachment);
      sphere.m_state = rigidbodyaspect::detail::States::Sliding;
      std::cout << "to sliding away from stress!" << std::endl;
    }

    else if (status == statechange::detail::StateChangeStatus::ToFree) {
      bookkeeping::clearAttachments(sphere);
      sphere.m_state = rigidbodyaspect::detail::States::Free;
      std::cout << "to freedom!" << std::endl;
    }
  }


  detail::StateChangeStatus checkSpPlStateChange(rbtypes::Sphere     &sphere,
                                                 rbtypes::FixedPlane &plane)
  {
    const auto p = sphere.frameOriginParent();
    const auto r = sphere.m_radius;
    const auto ds = sphere.m_ds;

    const auto n = Pl_n_q_pair(plane).first;
    const auto q = Pl_n_q_pair(plane).second;
    GM2Vector nr = n * r;
    const auto d = (q + nr) - p;

    //From Free to attached
    while(sphere.m_attachments.empty()){
      if(std::abs(blaze::inner(d,n))<DBL_EPSILON and
         blaze::inner(ds,n)<=0 and
         std::abs(blaze::inner(-nr,ds) - blaze::inner(ds,ds)) < DBL_EPSILON)
        return detail::StateChangeStatus::ToAtRest;
      else if(std::abs(blaze::inner(d,n))<DBL_EPSILON and
              blaze::inner(ds,n)<=0){
        return detail::StateChangeStatus::ToSliding;
      }
      else
        return detail::StateChangeStatus::NoChange;
    }

    //From Attached
    while(!sphere.m_attachments.empty()){
      auto uv_pair = closestpoint::calcPlaneClosestPoint(sphere, plane);
      if(blaze::inner(ds,n)>0)
        return detail::StateChangeStatus::ToFree;
      else{
        if(sphere.m_state == rigidbodyaspect::detail::States::Sliding){
          if(std::abs(blaze::inner(-nr,ds) - blaze::inner(ds,ds))<DBL_EPSILON)
            return detail::StateChangeStatus::ToAtRest;

          else
            return detail::StateChangeStatus::NoChange;
        }
        else if(sphere.m_state == rigidbodyaspect::detail::States::AtRest){
          if(std::abs(blaze::inner(-nr,ds) - blaze::inner(ds,ds))>=DBL_EPSILON)
            return detail::StateChangeStatus::ToSliding;
          else
            return detail::StateChangeStatus::NoChange;
        }
        else
          return detail::StateChangeStatus::NoChange;
      }
    }

    return detail::StateChangeStatus::NoChange;
  }

  detail::n_q_pair Pl_n_q_pair(rbtypes::FixedPlane &plane)
  {
    blaze::DynamicMatrix<blaze::StaticVector<double,4,0>,0> surface_eval;

    surface_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                                        rbtypes::FixedPlane::PSizeArray{1UL, 1UL});

    const auto u = blaze::subvector<0UL, 3UL>(surface_eval(1UL, 0UL));
    const auto v = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 1UL));
    const auto q = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 0UL));
    const auto n = blaze::normalize(blaze::cross(u, v));

    return detail::n_q_pair{n, q};
  }



  //SphereSphere
  void resolveSpSpStateChange(rbtypes::Sphere &sphere1,
                              rbtypes::Sphere &sphere2)
  {
    detail::StateChangeStatus status = checkSpSpStateChange(sphere1, sphere2);

    //Attachment definitions
    rigidbodyaspect::detail::attachment_pair
        sphere_attachment(&sphere2, rigidbodyaspect::detail::attachment_types::sphere);

    //State resolution
    if (status == statechange::detail::StateChangeStatus::ToAtRest){
      bookkeeping::addAttachments(sphere1, sphere_attachment);
      sphere1.m_state = rigidbodyaspect::detail::States::AtRest;
      std::cout << "to resting in peace!" << std::endl;
    }

    else if (status == statechange::detail::StateChangeStatus::ToSliding) {
      //auto n_d_pair = closestpoint::closestPointDistNorm(sphere, plane);
      bookkeeping::addAttachments(sphere1, sphere_attachment);
      sphere1.m_state = rigidbodyaspect::detail::States::Sliding;
      std::cout << "to sliding away from stress!" << std::endl;
    }

    else if (status == statechange::detail::StateChangeStatus::ToFree) {
      bookkeeping::clearAttachments(sphere1);
      sphere1.m_state = rigidbodyaspect::detail::States::Free;
      std::cout << "to freedom!" << std::endl;
    }
  }

  detail::StateChangeStatus checkSpSpStateChange(rbtypes::Sphere &sphere1,
                                                 rbtypes::Sphere &sphere2)
  {
    const auto p = sphere1.frameOriginParent();
    const auto r = sphere1.m_radius;
    const auto ds = sphere1.m_ds;

    const auto n = Sp_n_q_pair(sphere2).first;
    const auto q = Sp_n_q_pair(sphere2).second;
    const auto d = (q + (r * n)) - p;
    const auto nr = n * r;

    //From Free to attached
    while(sphere1.m_attachments.empty()){
      if(std::abs(blaze::inner(d,n))<DBL_EPSILON and
         blaze::inner(ds,n)<=0 and
         std::abs(blaze::inner(-nr,ds) - blaze::inner(ds,ds)) < DBL_EPSILON)
        return detail::StateChangeStatus::ToAtRest;
      else if(std::abs(blaze::inner(d,n))<DBL_EPSILON and
              blaze::inner(ds,n)<=0){
        return detail::StateChangeStatus::ToSliding;
      }
      else
        return detail::StateChangeStatus::NoChange;
    }
    //From Attached
    while(!sphere1.m_attachments.empty()){
      if(blaze::inner(ds,n)>0)
        return detail::StateChangeStatus::ToFree;
      else{
        if(sphere1.m_state == rigidbodyaspect::detail::States::Sliding){
          if(std::abs(blaze::inner(-nr,ds) - blaze::inner(ds,ds))<DBL_EPSILON)
            return detail::StateChangeStatus::ToAtRest;
          else
            return detail::StateChangeStatus::NoChange;
        }
        else if(sphere1.m_state == rigidbodyaspect::detail::States::AtRest){
          if(std::abs(blaze::inner(-nr,ds) - blaze::inner(ds,ds))>=DBL_EPSILON)
            return detail::StateChangeStatus::ToSliding;
          else
            return detail::StateChangeStatus::NoChange;
        }
        else
          return detail::StateChangeStatus::NoChange;
      }
    }

    return detail::StateChangeStatus::NoChange;
  }


  detail::n_q_pair Sp_n_q_pair(rbtypes::Sphere &sphere)
  {
    blaze::DynamicMatrix<blaze::StaticVector<double,4,0>,0> surface_eval;

    surface_eval = sphere.evaluateParent(rbtypes::Sphere::PSpacePoint{0.0, 0.0},
                                         rbtypes::Sphere::PSizeArray{1UL, 1UL});

    const auto u = blaze::subvector<0UL, 3UL>(surface_eval(1UL, 0UL));
    const auto v = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 1UL));
    const auto q = blaze::subvector<0UL, 3UL>(surface_eval(0UL, 0UL));
    const auto n = blaze::normalize(blaze::cross(u, v));

    return detail::n_q_pair{n, q};
  }


}   // namespace rigidbodyaspect::algorithms::statechange
