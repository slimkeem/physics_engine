#include "sortunique.h"

namespace rigidbodyaspect::algorithms::sortunique
{

  void driveSortUnique(collisiondetection::detail::SpherePlaneCollisionContainer  &sphere_plane_collisions,
                       collisiondetection::detail::SphereSphereCollisionContainer &sphere_sphere_collisions)
  {

    UniqueSphereSet unique_spheres;
    UniqueSphereSet non_unique_spheres;
    unique_spheres.clear();

    //Sphere X Plane Sort
    std::sort(std::begin(sphere_plane_collisions), std::end(sphere_plane_collisions),
              [](const auto &lhs, const auto &rhs)
    {
      return std::get<2>(lhs) > std::get<2>(rhs);
    });

    //Sphere X Sphere Sort
    std::sort(std::begin(sphere_sphere_collisions), std::end(sphere_sphere_collisions),
              [](const auto &lhs, const auto &rhs)
    {
      return std::get<2>(lhs) > std::get<2>(rhs);
    });


    //Take Unique Spheres across both Collisions
    auto sphere_plane_collisions_iterator  = sphere_plane_collisions.begin();
    auto sphere_sphere_collisions_iterator = sphere_sphere_collisions.begin();

    while (sphere_plane_collisions_iterator  != sphere_plane_collisions.end() &&
           sphere_sphere_collisions_iterator != sphere_sphere_collisions.end()) {

      const auto sphere_plane_collisions_dt       = std::get<2>(*sphere_plane_collisions_iterator);
      const auto sphere_sphere_collisions_dt      = std::get<2>(*sphere_sphere_collisions_iterator);
      const auto sphere_plane_collisions_sphere   = std::get<0>(*sphere_plane_collisions_iterator);
      const auto sphere_sphere_collisions_sphere  = std::get<0>(*sphere_sphere_collisions_iterator);

      if (sphere_plane_collisions_iterator != sphere_plane_collisions.end() and
          sphere_plane_collisions_dt > sphere_sphere_collisions_dt){
        if (unique_spheres.count(sphere_plane_collisions_sphere)) //check for existence
          non_unique_spheres.insert(sphere_plane_collisions_sphere);
        unique_spheres.insert(sphere_plane_collisions_sphere);
        sphere_plane_collisions_iterator++;
      }

      if (sphere_sphere_collisions_iterator != sphere_sphere_collisions.end() and
          sphere_plane_collisions_dt < sphere_sphere_collisions_dt){
        if (unique_spheres.count(sphere_sphere_collisions_sphere))
          non_unique_spheres.insert(sphere_sphere_collisions_sphere);
        unique_spheres.insert(sphere_sphere_collisions_sphere);
        sphere_sphere_collisions_iterator++;
      }
    }

    // Take Unique Spheres in Sphere X Plane Collisions
    const auto sphere_plane_unique_end = std::remove_if(
          sphere_plane_collisions.begin(), sphere_plane_collisions.end(),
          [&non_unique_spheres](const algorithms::collisiondetection::detail::SpherePlaneCollisionObject &sp_pl_col_obj){
      auto *sphere = std::get<0>(sp_pl_col_obj);
      if (non_unique_spheres.count(sphere))
        return true;
      non_unique_spheres.insert(sphere);
      return false;
    });
    // Physically Remoove
    sphere_plane_collisions.erase(sphere_plane_unique_end, sphere_plane_collisions.end());

    // Take Unique Spheres in Sphere X Sphere Collisions
    const auto sphere_sphere_unique_end = std::remove_if(
          sphere_sphere_collisions.begin(), sphere_sphere_collisions.end(),
          [&non_unique_spheres](const algorithms::collisiondetection::detail::SphereSphereCollisionObject &sp_sp_col_obj){
      auto *sphere1 = std::get<0>(sp_sp_col_obj);
      if (non_unique_spheres.count(sphere1))
        return true;
      non_unique_spheres.insert(sphere1);
      return false;
    });
    // Physically Remoove
    sphere_sphere_collisions.erase(sphere_sphere_unique_end, sphere_sphere_collisions.end());

    // DEBUG: Print out Collisions and Time
    for (auto s_pit = sphere_plane_collisions.begin(); s_pit != sphere_plane_collisions.end(); s_pit++){
      std::cout <<"Collision between Sphere: " << std::get<0>(*s_pit) <<
                  ", Plane: "                  << std::get<1>(*s_pit) <<
                  ", in Time: "                << std::get<2>(*s_pit).count() << std::endl;
    }

    for (auto s_sit = sphere_sphere_collisions.begin(); s_sit != sphere_sphere_collisions.end(); ++s_sit){
      std::cout <<"Collision between Sphere: " << std::get<0>(*s_sit) <<
                  ", Sphere: "                 << std::get<1>(*s_sit) <<
                  ", in Time: "                << std::get<2>(*s_sit).count() << std::endl;
    }

  }

}
