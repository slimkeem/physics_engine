#include "impactresponse.h"

namespace rigidbodyaspect::algorithms::impactresponse
{

  void
  computeSpherePlaneImpactResponse (rbtypes::Sphere     &sphere,
                                    rbtypes::FixedPlane &plane)
  {

      const auto pl_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                                                rbtypes::FixedPlane::PSizeArray{1UL, 1UL});

      const auto pl_u    = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
      const auto pl_v    = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
      const auto n       = blaze::normalize(blaze::cross(pl_u, pl_v));

      sphere.m_velocity  = (sphere.m_velocity - (2 * (blaze::inner(sphere.m_velocity,n)*n)))*0.95;

   }

  void
  computeSphereSphereImpactResponse (rbtypes::Sphere &sphere1,
                                     rbtypes::Sphere &sphere2)
  {

    auto p0                     = sphere1.frameOriginParent();
    auto p1                     = sphere2.frameOriginParent();
    auto p1_to_p0               = p1-p0;
    gmlib2::DVectorT<double> d  = p1_to_p0/blaze::length(p1_to_p0);

    auto liv_d                  = gmlib2::algorithms::linearIndependentVector(d);
    auto liv_dXd                = blaze::cross(liv_d,d);
    auto n                      = liv_dXd/blaze::length(liv_dXd);

    auto v0                     = sphere1.m_velocity;
    auto v1                     = sphere2.m_velocity;
    auto v0d                    = blaze::inner(v0,d);
    auto v1d                    = blaze::inner(v1,d);
    auto v0n                    = blaze::inner(v0,n);
    auto v1n                    = blaze::inner(v1,n);

    auto v0d_new = (((sphere1.m_mass - sphere2.m_mass)/(sphere1.m_mass + sphere2.m_mass))*v0d)
        + ((2*sphere2.m_mass/ (sphere1.m_mass + sphere2.m_mass))*v1d);
    auto v1d_new = (((sphere2.m_mass - sphere1.m_mass)/(sphere1.m_mass + sphere2.m_mass))*v1d)
        + ((2*sphere1.m_mass/ (sphere1.m_mass + sphere2.m_mass))*v0d);

    sphere1.m_velocity = ((v0n*n) + (v0d_new*d))*0.95;
    sphere2.m_velocity = ((v1n*n) + (v1d_new*d))*0.95;
  }

  void computeSphereCylinderImpactResponse(rbtypes::Sphere &sphere, rbtypes::FixedCylinder &cylinder)
  {

    const auto pl_eval = cylinder.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                                                 rbtypes::FixedPlane::PSizeArray{1UL, 1UL});

    const auto pl_u    = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
    const auto pl_v    = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
    const auto q       = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));
    const auto vec     = blaze::normalize(blaze::cross(pl_u, pl_v));
    auto p             = sphere.frameOriginParent();
    auto r             = cylinder.m_radius;
    auto d             = (q + vec*r) - p;
    auto c             = blaze::normalize(blaze::cross(q, pl_v));
    auto c_new         = blaze::cross(c, d);
    auto n             = blaze::cross(c_new, c)/blaze::length(blaze::cross(c_new, c));


    sphere.m_velocity  = (sphere.m_velocity - (2 * (blaze::inner(sphere.m_velocity,n)*n)))*0.95;


  }

}
