#ifndef RIGIDBODYASPECT_GENERICSIMJOB_H
#define RIGIDBODYASPECT_GENERICSIMJOB_H

#include "../types.h"

// qt
#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>


namespace rigidbodyaspect
{

  class RigidBodyAspect;

  class GenericSimJob : public Qt3DCore::QAspectJob {
  public:
    GenericSimJob(RigidBodyAspect* aspect);

    void setFrameTimeDt(seconds_type dt);

  private:
    RigidBodyAspect* m_aspect;
    seconds_type     m_dt;

    // QAspectJob interface
  public:
    void run() override;
  };

  using GenericSimJobPtr = QSharedPointer<GenericSimJob>;

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_GENERICSIMJOB_H
