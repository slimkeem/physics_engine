#include "genericcolliderjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/collisiondetection.h"
#include "../algorithms/cacheproperty.h"
#include "../algorithms/impactresponse.h"
#include "../algorithms/simulation.h"
#include "../algorithms/sortunique.h"
#include "../algorithms/collisionresolution.h"
#include "../algorithms/collisioninsertion.h"


// qt
#include <QDebug>

namespace rigidbodyaspect
{

  GenericColliderJob::GenericColliderJob(RigidBodyAspect* aspect) : m_aspect{aspect} {}

  void GenericColliderJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void GenericColliderJob::run()
  {
    auto& rigid_bodies = m_aspect->rigidBodies();
    algorithms::collisiondetection::detail::SpherePlaneCollisionContainer sp_pl_col;
    algorithms::collisiondetection::detail::SphereSphereCollisionContainer sp_sp_col;
//    algorithms::collisiondetection::detail::SphereCylinderCollisionContainer sp_cy_col;


    // 1. Compute Initial Cache Properties
    for (auto& sphere : rigid_bodies.spheres()) {
      sphere.m_dt_tp    = seconds_type(0);
      algorithms::cacheproperty::calcProperties(sphere, m_aspect, m_dt);
    }


    // 2. Detect Initial Collisions and Insert to container
    for (auto sphere1_it  = rigid_bodies.spheres().begin();
         sphere1_it != rigid_bodies.spheres().end(); ++sphere1_it){
      for (auto &fixed_plane : rigid_bodies.fixedPlanes()) {
        const auto collision_check = algorithms::collisiondetection::detectCollisionSphereFixedplane
            (*sphere1_it, fixed_plane, m_dt).second;
        const auto impact_time =     algorithms::collisiondetection::detectCollisionSphereFixedplane
            (*sphere1_it, fixed_plane, m_dt).first;
        if (collision_check == algorithms::collisiondetection::detail::ColliderStatus::Collision){
          if(sphere1_it->m_attachments.empty())
            sp_pl_col.emplace_back(&*sphere1_it, &fixed_plane, impact_time);
          for (auto &attachment : sphere1_it->m_attachments){
            auto attachment_type = attachment.second;
            if (attachment_type == detail::attachment_types::plane){
              auto attached_plane = static_cast<rbtypes::FixedPlane*>(attachment.first);
              if (&fixed_plane == attached_plane)
                continue;
              sp_pl_col.emplace_back(&*sphere1_it, &fixed_plane, impact_time);
            }
          }
        }
      }
//      for (auto &fixed_cylinder : rigid_bodies.fixedCylinders()) {
//        const auto collision_check = algorithms::collisiondetection::detectCollisionSphereCylinder
//            (*sphere1_it, fixed_cylinder, m_dt).second;
//        const auto impact_time =     algorithms::collisiondetection::detectCollisionSphereCylinder
//            (*sphere1_it, fixed_cylinder, m_dt).first;
//        if (collision_check == algorithms::collisiondetection::detail::ColliderStatus::Collision){
//          if(sphere1_it->m_attachments.empty())
//            sp_cy_col.emplace_back(&*sphere1_it, &fixed_cylinder, impact_time);
//          for (auto &attachment : sphere1_it->m_attachments){
//            auto attachment_type = attachment.second;
//            if (attachment_type == detail::attachment_types::cylinder){
//              auto attached_cylinder = static_cast<rbtypes::FixedCylinder*>(attachment.first);
//              if (&fixed_cylinder == attached_cylinder)
//                continue;
//              sp_cy_col.emplace_back(&*sphere1_it, &fixed_cylinder, impact_time);
//            }
//          }
//        }
//      }
      for (auto sphere2_it  = sphere1_it+1; sphere2_it != rigid_bodies.spheres().end(); ++sphere2_it){
        const auto collision_check = algorithms::collisiondetection::detectCollisionSphereSphere
            (*sphere1_it, *sphere2_it, m_dt).second;
        const auto impact_time =     algorithms::collisiondetection::detectCollisionSphereSphere
            (*sphere1_it, *sphere2_it, m_dt).first;
        if (collision_check == algorithms::collisiondetection::detail::ColliderStatus::Collision){
          if(sphere1_it->m_attachments.empty())
            sp_sp_col.emplace_back(&*sphere1_it, &*sphere2_it, impact_time);
          for (auto &attachment : sphere1_it->m_attachments){
            auto attachment_type = attachment.second;
            if (attachment_type == detail::attachment_types::sphere){
              auto attached_sphere = static_cast<rbtypes::Sphere*>(attachment.first);
              if (&*sphere2_it == attached_sphere)
                continue;
              sp_sp_col.emplace_back(&*sphere1_it, &*sphere2_it, impact_time);
            }
          }
        }
      }
    }


    // 3. Sort Collisions and make unique
    algorithms::sortunique::driveSortUnique(sp_pl_col, sp_sp_col);


    // 4. Work on individual Sphere Plane collisions
    while (!sp_pl_col.empty() or !sp_sp_col.empty()) {
      if (!sp_pl_col.empty() and sp_sp_col.empty())
        algorithms::collisionresolution::resolveSpherePlaneCollision(sp_pl_col,m_aspect, m_dt);
      else if (sp_pl_col.empty() and !sp_sp_col.empty())
        algorithms::collisionresolution::resolveSphereSphereCollision(sp_sp_col,m_aspect, m_dt);
      else {
        const auto& sp_pl_col_tp = std::get<2>(sp_pl_col.back());
        const auto& sp_sp_col_tp = std::get<2>(sp_sp_col.back());
        if(sp_pl_col_tp < sp_sp_col_tp)
          algorithms::collisionresolution::resolveSpherePlaneCollision(sp_pl_col,m_aspect, m_dt);
        algorithms::collisionresolution::resolveSphereSphereCollision(sp_sp_col,m_aspect, m_dt);
      }

      algorithms::sortunique::driveSortUnique(sp_pl_col, sp_sp_col);
    }


    // 5. Simulate all moving objects
    for (auto& sphere : rigid_bodies.spheres()){
      auto dt_rem = m_dt - sphere.m_dt_tp;
      auto dt_rem_factor = dt_rem/m_dt;
      if (dt_rem_factor > 0.0 && dt_rem_factor <= 1.0){
  //      for (auto &fixed_plane : rigid_bodies.fixedPlanes()) {
  //        if(sphere.m_ds==GM2Vector{0,0,0})
  //          algorithms::statechange::resolveSpPlStateChange(sphere, fixed_plane);
  //      }
        algorithms::simulation::simulateGenericMovingBody(sphere, dt_rem_factor);
      }
    }


    ///////////////////////////////


  }

}   // namespace rigidbodyaspect
