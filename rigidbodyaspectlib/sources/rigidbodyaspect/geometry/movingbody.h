#ifndef RIGIDBODYASPECT_MOVINGBODY_H
#define RIGIDBODYASPECT_MOVINGBODY_H

#include "../types.h"
// stl
#include <unordered_set>

namespace rigidbodyaspect
{

  namespace detail
  {
    enum class States { Free, AtRest, Sliding };
    enum class attachment_types { plane, sphere, cylinder};
    using attachment_pair  = std::pair<void*, attachment_types>;
  }

  class MovingBody : public GM2SpaceObjectType  {

  public:

    // property members
    Unit_Type                                   m_mass;
    GM2Vector                                   m_velocity;
    Qt3DCore::QNodeId                           m_env_id;

    // derived members
    GM2Vector                                   m_ds;
    GM2Vector                                   m_acceleration;
    seconds_type                                m_dt_tp;

    //state change members
    detail::States                              m_state;
    std::vector<detail::attachment_pair>        m_attachments;

  };

}   // namespace rigidbodyaspect

#endif // RIGIDBODYASPECT_MOVINGBODY_H

