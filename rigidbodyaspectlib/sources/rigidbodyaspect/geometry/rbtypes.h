#ifndef RIGIDBODYASPECT_RBTYPES_H
#define RIGIDBODYASPECT_RBTYPES_H

#include "movingbody.h"
#include "fixedbody.h"

// stl
#include <unordered_set>

namespace rigidbodyaspect {

  namespace rbtypes {

      using Sphere        = gmlib2::parametric::PSphere<MovingBody>;
      using FixedCylinder = customshapes::PCylinder<FixedBody>;
      using FixedPlane    = gmlib2::parametric::PPlane<FixedBody>;
  }

//  using set_container = std::unordered_set<rbtypes::FixedPlane*>;

}



#endif // RIGIDBODYASPECT_RBTYPES_H
