constexpr auto rba_name = "rigidbodyaspect";

#include "rigidbodyaspect.h"

#include "../frontend/simulatorsettings.h"
#include "simulatorsettingsbackend.h"

#include "../frontend/environment.h"
#include "environmentbackend.h"

#include "../frontend/spherecontroller.h"
#include "spherecontrollerbackend.h"

#include "../frontend/cylindercontroller.h"
#include "cylindercontrollerbackend.h"

#include "../frontend/planecontroller.h"
#include "planecontrollerbackend.h"

#include <QAbstractAspect>
using namespace Qt3DCore;

//stl
#include <chrono>
using namespace std::literals::chrono_literals;

namespace rigidbodyaspect
{

  RigidBodyAspect::RigidBodyAspect(QObject* parent)
    : Qt3DCore::QAbstractAspect(parent),
      m_colliderworker{GenericColliderJobPtr::create(this)},
//      m_simworker{GenericSimJobPtr::create(this)},
      m_updateworker{GenericUpdateJobPtr::create(this)}
  {

    qDebug() << "Constructing RigidBodyAspect";

    auto sim_settings_mapper
      = QSharedPointer<SimulatorSettingsBackendMapper>::create(this);
    registerBackendType<SimulatorSettings>(sim_settings_mapper);

    auto environment_mapper
      = QSharedPointer<EnvironmentBackendMapper>::create(this);
    registerBackendType<Environment>(environment_mapper);

    auto sphere_mapper
      = QSharedPointer<SphereControllerBackendMapper>::create(this);
    registerBackendType<SphereController>(sphere_mapper);

    auto cylinder_mapper
      = QSharedPointer<CylinderControllerBackendMapper>::create(this);
    registerBackendType<CylinderController>(cylinder_mapper);

    auto plane_mapper
      = QSharedPointer<PlaneControllerBackendMapper>::create(this);
    registerBackendType<PlaneController>(plane_mapper);


//    m_simworker->addDependency(m_colliderworker.toWeakRef());
//    m_updateworker->addDependency(m_simworker.toWeakRef());
      m_updateworker->addDependency(m_colliderworker.toWeakRef());

  }

  SimulatorSettingsBackend* RigidBodyAspect::constructSimulatorSettingsBackend(QNodeId id)
  {

    if(m_simulator_settings_backend not_eq nullptr) {
      qWarning() << "Simulator settings already specified";
      return nullptr;
    }

    m_simulator_settings_backend.reset(new SimulatorSettingsBackend);
    m_simulator_settings_backend_peerid = id;

    return m_simulator_settings_backend.get();
  }

  SimulatorSettingsBackend*RigidBodyAspect::simulatorSettingsBackend(QNodeId id)
  {
    if (m_simulator_settings_backend not_eq nullptr
        and m_simulator_settings_backend_peerid == id)
      return m_simulator_settings_backend.get();

    return nullptr;
  }

  void RigidBodyAspect::releaseSimulatorSettingsBackend(QNodeId id)
  {
    if (m_simulator_settings_backend not_eq nullptr
        and m_simulator_settings_backend_peerid == id)
      m_simulator_settings_backend.reset(nullptr);
  }

  void RigidBodyAspect::addEnvironmentBackend(QNodeId id, EnvironmentBackend* backend)
  {
    m_environment_backends.insert(id,backend);
  }

  EnvironmentBackend*RigidBodyAspect::environmentBackend(QNodeId id)
  {
    return m_environment_backends.value(id,nullptr);
  }

  EnvironmentBackend*RigidBodyAspect::takeEnvironmentBackend(QNodeId id)
  {
    return m_environment_backends.take(id);
  }

  const QHash<QNodeId, EnvironmentBackend*>&RigidBodyAspect::environmentBackends() const
  {
    return m_environment_backends;
  }


  void RigidBodyAspect::addSphereControllerBackend(
    Qt3DCore::QNodeId id, SphereControllerBackend* backend)
  {
    m_spherecontroller_backends.insert(id, backend);
  }

  SphereControllerBackend*
  RigidBodyAspect::sphereControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_spherecontroller_backends.value(id, nullptr);
  }

  SphereControllerBackend*
  RigidBodyAspect::takeSphereControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_spherecontroller_backends.take(id);
  }

  const QHash<Qt3DCore::QNodeId, SphereControllerBackend*>&
  RigidBodyAspect::sphereControllerBackends() const
  {
    return m_spherecontroller_backends;
  }

  void RigidBodyAspect::addCylinderControllerBackend(
    Qt3DCore::QNodeId id, CylinderControllerBackend* backend)
  {
    m_cylindercontroller_backends.insert(id, backend);
  }

  CylinderControllerBackend*
  RigidBodyAspect::cylinderControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_cylindercontroller_backends.value(id, nullptr);
  }

  CylinderControllerBackend*
  RigidBodyAspect::takeCylinderControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_cylindercontroller_backends.take(id);
  }

  const QHash<Qt3DCore::QNodeId, CylinderControllerBackend*>&
  RigidBodyAspect::cylinderControllerBackends() const
  {
    return m_cylindercontroller_backends;
  }

  void RigidBodyAspect::addPlaneControllerBackend(
    Qt3DCore::QNodeId id, PlaneControllerBackend* backend)
  {
    m_planecontroller_backends.insert(id, backend);
  }

  PlaneControllerBackend*
  RigidBodyAspect::planeControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_planecontroller_backends.value(id, nullptr);
  }

  PlaneControllerBackend*
  RigidBodyAspect::takePlaneControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_planecontroller_backends.take(id);
  }

  const QHash<Qt3DCore::QNodeId, PlaneControllerBackend*>&
  RigidBodyAspect::planeControllerBackends() const
  {
    return m_planecontroller_backends;
  }

  RigidBodyContainer& RigidBodyAspect::rigidBodies()
  {
    return m_rigid_bodies;
  }



  QVector<Qt3DCore::QAspectJobPtr> RigidBodyAspect::jobsToExecute(qint64 time)
  {

    //const auto time_dt    = time - m_last_time;
    //m_last_time           = time;

    if (not m_simulator_settings_backend
        or not m_simulator_settings_backend->runStatus())
      return{};

    //static constexpr auto time_resolution = 1000000000.0;
    //const seconds_type dt = seconds_type(time_dt / time_resolution);

    const seconds_type dt = 16ms;


    // Collider worker
    m_colliderworker->setFrameTimeDt(dt);

    // Update worker
//    m_simworker->setFrameTimeDt(dt);

//    return {m_colliderworker,m_simworker,m_updateworker};
    return {m_colliderworker,m_updateworker};
  }


}   // namespace rigidbodyaspect



QT3D_REGISTER_NAMESPACED_ASPECT(rba_name, rigidbodyaspect, RigidBodyAspect)
