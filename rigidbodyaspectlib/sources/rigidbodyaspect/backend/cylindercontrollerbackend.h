#ifndef CYLINDERCONTROLLERBACKEND_H
#define CYLINDERCONTROLLERBACKEND_H


#include "../types.h"

#include "../geometry/rigidbodycontainer.h"

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QBackendNode>

namespace rigidbodyaspect
{

  class CylinderControllerBackend : public Qt3DCore::QBackendNode {
  public:
    CylinderControllerBackend(RigidBodyContainer& rigid_bodies);
    ~CylinderControllerBackend() override;

    void queueFrontendUpdate();

  private:
    RigidBodyDynamicsType m_dynamics_type;
    QMatrix3x3            m_dup_frame;
    float                 m_radius;
    float                 m_height;
    RigidBodyContainer    &m_rigid_bodies;
    Qt3DCore::QNodeId     m_environment_id;


    void setRadius( float radius);
    void setHeight(float height);
    void setDupFrame( const QMatrix3x3 dup_frame );

    void setDynamicsType( const RigidBodyDynamicsType& dynamics_type );
    void setEnvironmentId( const Qt3DCore::QNodeId& id );


    rbtypes::FixedCylinder& cylinder();

    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
  };


  class RigidBodyAspect;

  class CylinderControllerBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit CylinderControllerBackendMapper(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;
  };

}   // namespace rigidbodyaspect

#endif // CYLINDERCONTROLLERBACKEND_H
