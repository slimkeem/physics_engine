#include "cylindercontrollerbackend.h"

#include "rigidbodyaspect.h"
#include "../frontend/cylindercontroller.h"
#include "../utils.h"

namespace rigidbodyaspect
{

  CylinderControllerBackend::CylinderControllerBackend(RigidBodyContainer& rigid_bodies)
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite),
      m_rigid_bodies{rigid_bodies}
  {
  }

  CylinderControllerBackend::~CylinderControllerBackend()
  {
    m_rigid_bodies.destroyFixedCylinder(peerId());
  }

  void CylinderControllerBackend::queueFrontendUpdate()
  {
    auto aFrame = m_rigid_bodies.fixedCylinder(peerId()).aSpaceFrameParent();

    // Convert data to sendable frame
    QMatrix3x3 q_dupframe;
    for (auto i = 0UL; i < 3UL; ++i) {
      q_dupframe(int(i), 0) = float(aFrame(i, 0UL));   // dir
      q_dupframe(int(i), 1) = float(aFrame(i, 2UL));   // up
      q_dupframe(int(i), 2) = float(aFrame(i, 3UL));   // origin
    }

    // Send data
    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("dupframe_BE"));
    e->setValue(QVariant::fromValue(q_dupframe));
    notifyObservers(e);
  }

  void CylinderControllerBackend::setRadius(float radius)
  {
    if (m_radius == radius) return;

    m_radius = radius;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    m_rigid_bodies.fixedCylinder(peerId()).m_radius = double(radius);
  }

  void CylinderControllerBackend::setHeight(float height)
  {
    if (m_height == height) return;

    m_height = height;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    m_rigid_bodies.fixedCylinder(peerId()).m_height = double(height);
  }

  void CylinderControllerBackend::setDupFrame(const QMatrix3x3 dup_frame)
  {
    if (qFuzzyCompare(dup_frame, m_dup_frame)) return;

    m_dup_frame = dup_frame;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    m_rigid_bodies.fixedCylinder(peerId()).setFrameParent(dir, up, pos);
  }

  void CylinderControllerBackend::setDynamicsType(
    const RigidBodyDynamicsType& dynamics_type)
  {
    if (m_dynamics_type == dynamics_type) return;
  }

  void CylinderControllerBackend::setEnvironmentId(const Qt3DCore::QNodeId& id)
  {
    if (m_environment_id == id) return;

    m_environment_id = id;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    m_rigid_bodies.fixedCylinder(peerId()).m_env_id = m_environment_id;
  }

  void CylinderControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange
      = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<CylinderInitialData>>(
        change);
    const auto& data = typedChange->data;
    m_dynamics_type  = data.m_dynamics_type;
    m_dup_frame      = data.m_dup_frame;
    m_radius         = data.m_radius;
    m_environment_id = data.m_environment_id;
    m_height       = data.m_height;

    m_rigid_bodies.constructFixedCylinder(peerId());
    auto& cylinder = m_rigid_bodies.fixedCylinder(peerId());

    // geometric properties
    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    cylinder.setFrameParent(dir, up, pos);
    cylinder.m_radius = double(m_radius);
    cylinder.m_env_id = m_environment_id;
    cylinder.m_height = double(m_height);
  }

  void
  CylinderControllerBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {
    if (e->type() == Qt3DCore::PropertyUpdated) {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      if (change->propertyName() == QByteArrayLiteral("dupFrame"))
        setDupFrame(change->value().value<QMatrix3x3>());
      else if (change->propertyName() == QByteArrayLiteral("dynamicsType"))
        setDynamicsType(change->value().value<RigidBodyDynamicsType>());
      else if (change->propertyName() == QByteArrayLiteral("environment"))
        setEnvironmentId(change->value().value<Qt3DCore::QNodeId>());
      else if (change->propertyName() == QByteArrayLiteral("radius"))
        setRadius(change->value().toFloat());
      else if (change->propertyName() == QByteArrayLiteral("height"))
        setHeight(change->value().toFloat());
    }

    QBackendNode::sceneChangeEvent(e);
  }

  CylinderControllerBackendMapper::CylinderControllerBackendMapper(
    RigidBodyAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* CylinderControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new CylinderControllerBackend(m_aspect->rigidBodies());
    m_aspect->addCylinderControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  CylinderControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->cylinderControllerBackend(id);
  }

  void CylinderControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takeCylinderControllerBackend(id);
  }

}   // namespace rigidbodyaspect
